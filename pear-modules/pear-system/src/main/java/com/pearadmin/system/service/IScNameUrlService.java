package com.pearadmin.system.service;

import java.util.List;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.ScNameUrl;

/**
 * 考勤人员管理Service接口
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
public interface IScNameUrlService 
{
    /**
     * 查询考勤人员管理
     * 
     * @param id 考勤人员管理ID
     * @return 考勤人员管理
     */
    ScNameUrl selectScNameUrlById(Long id);


    /**
    * 查询考勤人员管理
     * @param ${classsName} 考勤人员管理
     * @param pageDomain
     * @return 考勤人员管理 分页集合
     * */
    PageInfo<ScNameUrl> selectScNameUrlPage(ScNameUrl scNameUrl, PageDomain pageDomain);

    /**
     * 查询考勤人员管理列表
     * 
     * @param scNameUrl 考勤人员管理
     * @return 考勤人员管理集合
     */
    List<ScNameUrl> selectScNameUrlList(ScNameUrl scNameUrl);

    /**
     * 新增考勤人员管理
     * 
     * @param scNameUrl 考勤人员管理
     * @return 结果
     */
    int insertScNameUrl(ScNameUrl scNameUrl);

    /**
     * 修改考勤人员管理
     * 
     * @param scNameUrl 考勤人员管理
     * @return 结果
     */
    int updateScNameUrl(ScNameUrl scNameUrl);

    /**
     * 批量删除考勤人员管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteScNameUrlByIds(String[] ids);

    /**
     * 删除考勤人员管理信息
     * 
     * @param id 考勤人员管理ID
     * @return 结果
     */
    int deleteScNameUrlById(Long id);

}
