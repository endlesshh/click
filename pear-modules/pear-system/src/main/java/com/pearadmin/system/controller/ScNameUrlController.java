package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.system.domain.ScNameUrl;
import com.pearadmin.common.tools.string.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.system.domain.SysUser;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.pearadmin.system.service.IScNameUrlService;

import java.time.LocalDateTime;

/**
 * 考勤人员管理Controller
 *
 * @author shiqiang
 * @date 2021-05-14
 */
@RestController
@RequestMapping("/system/nameUrl")
@Api(tags = {"考勤人员"})
public class ScNameUrlController extends BaseController
{
    private String prefix = "system/nameUrl";

    @Autowired
    private IScNameUrlService scNameUrlService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/system/nameUrl/main','system:nameUrl:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询考勤人员管理列表
     */
    @ResponseBody
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/system/nameUrl/data','system:nameUrl:data')")
    public ResultTable list(@ModelAttribute ScNameUrl scNameUrl, PageDomain pageDomain)
    {
        PageInfo<ScNameUrl> pageInfo = scNameUrlService.selectScNameUrlPage(scNameUrl,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增考勤人员管理
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/system/nameUrl/add','system:nameUrl:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存考勤人员管理
     */
    @ResponseBody
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/system/nameUrl/add','system:nameUrl:add')")
    public Result save(@RequestBody ScNameUrl scNameUrl)
    {
        SysUser sysUser = (SysUser)SecurityUtil.currentUserObj();
        scNameUrl.setCreateTime(LocalDateTime.now());
        scNameUrl.setCreateBy(sysUser.getUserId());
        scNameUrl.setCreateName(sysUser.getUsername());
        return decide(scNameUrlService.insertScNameUrl(scNameUrl));
    }

    /**
     * 修改考勤人员管理
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/system/nameUrl/edit','system:nameUrl:edit')")
    public ModelAndView edit(Long id, ModelMap mmap)
    {
        ScNameUrl scNameUrl = scNameUrlService.selectScNameUrlById(id);
        mmap.put("scNameUrl", scNameUrl);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存考勤人员管理
     */
    @ResponseBody
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/system/nameUrl/edit','system:nameUrl:edit')")
    public Result update(@RequestBody ScNameUrl scNameUrl)
    {
        SysUser sysUser = (SysUser)SecurityUtil.currentUserObj();
        scNameUrl.setUpdateTime(LocalDateTime.now());
        scNameUrl.setUpdateBy(sysUser.getUserId());
        scNameUrl.setUpdateName(sysUser.getUsername());
        return decide(scNameUrlService.updateScNameUrl(scNameUrl));
    }

    /**
     * 删除考勤人员管理
     */
    @ResponseBody
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/system/nameUrl/remove','system:nameUrl:remove')")
    public Result batchRemove(String ids)
    {
        return decide(scNameUrlService.deleteScNameUrlByIds(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @ResponseBody
    @DeleteMapping("/remove/{id}")
    @PreAuthorize("hasPermission('/system/nameUrl/remove','system:nameUrl:remove')")
    public Result remove(@PathVariable("id") Long id)
    {
        return decide(scNameUrlService.deleteScNameUrlById(id));
    }

    /**
     * Describe: 更换头像
     * Param: null
     * Return: ModelAndView
     */
    @GetMapping("profile")
    public ModelAndView profile(Model model){
        return jumpPage(prefix + "/profile");
    }
}
