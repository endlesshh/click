package com.pearadmin.system.service;

import java.util.List;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.system.domain.ScLastPeopleTime;

/**
 * 记录考勤信息Service接口
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
public interface IScLastPeopleTimeService 
{
    /**
     * 查询记录考勤信息
     * 
     * @param id 记录考勤信息ID
     * @return 记录考勤信息
     */
    ScLastPeopleTime selectScLastPeopleTimeById(Long id);


    /**
    * 查询记录考勤信息
     * @param ${classsName} 记录考勤信息
     * @param pageDomain
     * @return 记录考勤信息 分页集合
     * */
    PageInfo<ScLastPeopleTime> selectScLastPeopleTimePage(ScLastPeopleTime scLastPeopleTime, PageDomain pageDomain);

    /**
     * 查询记录考勤信息列表
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 记录考勤信息集合
     */
    List<ScLastPeopleTime> selectScLastPeopleTimeList(ScLastPeopleTime scLastPeopleTime);

    /**
     * 新增记录考勤信息
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 结果
     */
    int insertScLastPeopleTime(ScLastPeopleTime scLastPeopleTime);

    /**
     * 修改记录考勤信息
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 结果
     */
    int updateScLastPeopleTime(ScLastPeopleTime scLastPeopleTime);

    /**
     * 批量删除记录考勤信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteScLastPeopleTimeByIds(String[] ids);

    /**
     * 删除记录考勤信息信息
     * 
     * @param id 记录考勤信息ID
     * @return 结果
     */
    int deleteScLastPeopleTimeById(Long id);

}
