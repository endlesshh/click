package com.pearadmin.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.system.domain.ScNameUrl;

/**
 * 考勤人员管理Mapper接口
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Mapper
public interface ScNameUrlMapper 
{
    /**
     * 查询考勤人员管理
     * 
     * @param id 考勤人员管理ID
     * @return 考勤人员管理
     */
    public ScNameUrl selectScNameUrlById(Long id);

    /**
     * 查询考勤人员管理列表
     * 
     * @param scNameUrl 考勤人员管理
     * @return 考勤人员管理集合
     */
    List<ScNameUrl> selectScNameUrlList(ScNameUrl scNameUrl);

    /**
     * 新增考勤人员管理
     * 
     * @param scNameUrl 考勤人员管理
     * @return 结果
     */
    int insertScNameUrl(ScNameUrl scNameUrl);

    /**
     * 修改考勤人员管理
     * 
     * @param scNameUrl 考勤人员管理
     * @return 结果
     */
    int updateScNameUrl(ScNameUrl scNameUrl);

    /**
     * 删除考勤人员管理
     * 
     * @param id 考勤人员管理ID
     * @return 结果
     */
    int deleteScNameUrlById(Long id);

    /**
     * 批量删除考勤人员管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteScNameUrlByIds(String[] ids);

}
