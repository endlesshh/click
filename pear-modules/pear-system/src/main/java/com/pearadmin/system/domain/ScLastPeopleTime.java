package com.pearadmin.system.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 记录考勤信息对象 sc_last_people_time
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Data
public class ScLastPeopleTime extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 日期 */
    private String time;

    /** 考勤开始时间 */
    private String start;

    /** 考勤结束时间 */
    private String stop;

    /** 应到人数 */
    private String people1;

    /** 实到人数 */
    private String people2;

    /** 考勤人员列表 */
    private String text;

}
