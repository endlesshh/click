package com.pearadmin.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.system.domain.ScLastPeopleTime;

/**
 * 记录考勤信息Mapper接口
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Mapper
public interface ScLastPeopleTimeMapper 
{
    /**
     * 查询记录考勤信息
     * 
     * @param id 记录考勤信息ID
     * @return 记录考勤信息
     */
    public ScLastPeopleTime selectScLastPeopleTimeById(Long id);

    /**
     * 查询记录考勤信息列表
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 记录考勤信息集合
     */
    List<ScLastPeopleTime> selectScLastPeopleTimeList(ScLastPeopleTime scLastPeopleTime);

    /**
     * 新增记录考勤信息
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 结果
     */
    int insertScLastPeopleTime(ScLastPeopleTime scLastPeopleTime);

    /**
     * 修改记录考勤信息
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 结果
     */
    int updateScLastPeopleTime(ScLastPeopleTime scLastPeopleTime);

    /**
     * 删除记录考勤信息
     * 
     * @param id 记录考勤信息ID
     * @return 结果
     */
    int deleteScLastPeopleTimeById(Long id);

    /**
     * 批量删除记录考勤信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteScLastPeopleTimeByIds(String[] ids);

}
