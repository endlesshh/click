package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.system.domain.ScLastPeopleTime;
import com.pearadmin.common.tools.string.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.system.domain.SysUser;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.pearadmin.system.service.IScLastPeopleTimeService;

import java.time.LocalDateTime;

/**
 * 记录考勤信息Controller
 *
 * @author shiqiang
 * @date 2021-05-14
 */
@RestController
@RequestMapping("/system/lastPeopleTime")
@Api(tags = {"考勤记录"})
public class ScLastPeopleTimeController extends BaseController
{
    private String prefix = "system/lastPeopleTime";

    @Autowired
    private IScLastPeopleTimeService scLastPeopleTimeService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/system/lastPeopleTime/main','system:lastPeopleTime:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询记录考勤信息列表
     */
    @ResponseBody
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/system/lastPeopleTime/data','system:lastPeopleTime:data')")
    public ResultTable list(@ModelAttribute ScLastPeopleTime scLastPeopleTime, PageDomain pageDomain)
    {
        PageInfo<ScLastPeopleTime> pageInfo = scLastPeopleTimeService.selectScLastPeopleTimePage(scLastPeopleTime,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增记录考勤信息
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/system/lastPeopleTime/add','system:lastPeopleTime:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存记录考勤信息
     */
    @ResponseBody
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/system/lastPeopleTime/add','system:lastPeopleTime:add')")
    public Result save(@RequestBody ScLastPeopleTime scLastPeopleTime)
    {
        SysUser sysUser = (SysUser)SecurityUtil.currentUserObj();
        scLastPeopleTime.setCreateTime(LocalDateTime.now());
        scLastPeopleTime.setCreateBy(sysUser.getUserId());
        scLastPeopleTime.setCreateName(sysUser.getUsername());
        return decide(scLastPeopleTimeService.insertScLastPeopleTime(scLastPeopleTime));
    }

    /**
     * 修改记录考勤信息
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/system/lastPeopleTime/edit','system:lastPeopleTime:edit')")
    public ModelAndView edit(Long id, ModelMap mmap)
    {
        ScLastPeopleTime scLastPeopleTime = scLastPeopleTimeService.selectScLastPeopleTimeById(id);
        mmap.put("scLastPeopleTime", scLastPeopleTime);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存记录考勤信息
     */
    @ResponseBody
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/system/lastPeopleTime/edit','system:lastPeopleTime:edit')")
    public Result update(@RequestBody ScLastPeopleTime scLastPeopleTime)
    {
        SysUser sysUser = (SysUser)SecurityUtil.currentUserObj();
        scLastPeopleTime.setUpdateTime(LocalDateTime.now());
        scLastPeopleTime.setUpdateBy(sysUser.getUserId());
        scLastPeopleTime.setUpdateName(sysUser.getUsername());
        return decide(scLastPeopleTimeService.updateScLastPeopleTime(scLastPeopleTime));
    }

    /**
     * 删除记录考勤信息
     */
    @ResponseBody
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/system/lastPeopleTime/remove','system:lastPeopleTime:remove')")
    public Result batchRemove(String ids)
    {
        return decide(scLastPeopleTimeService.deleteScLastPeopleTimeByIds(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @ResponseBody
    @DeleteMapping("/remove/{id}")
    @PreAuthorize("hasPermission('/system/lastPeopleTime/remove','system:lastPeopleTime:remove')")
    public Result remove(@PathVariable("id") Long id)
    {
        return decide(scLastPeopleTimeService.deleteScLastPeopleTimeById(id));
    }



}
