package com.pearadmin.system.service.impl;

import java.util.List;
import java.util.ArrayList;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pearadmin.system.mapper.ScTimeMapper;
import com.pearadmin.system.domain.ScTime;
import com.pearadmin.system.service.IScTimeService;

/**
 * 设置考勤时段Service业务层处理
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Service
public class ScTimeServiceImpl implements IScTimeService 
{
    @Autowired
    private ScTimeMapper scTimeMapper;

    /**
     * 查询设置考勤时段
     * 
     * @param id 设置考勤时段ID
     * @return 设置考勤时段
     */
    @Override
    public ScTime selectScTimeById(Long id)
    {
        return scTimeMapper.selectScTimeById(id);
    }

    /**
     * 查询设置考勤时段列表
     * 
     * @param scTime 设置考勤时段
     * @return 设置考勤时段
     */
    @Override
    public List<ScTime> selectScTimeList(ScTime scTime)
    {
        return scTimeMapper.selectScTimeList(scTime);
    }

    /**
     * 查询设置考勤时段
     * @param scTime 设置考勤时段
     * @param pageDomain
     * @return 设置考勤时段 分页集合
     * */
    @Override
    public PageInfo<ScTime> selectScTimePage(ScTime scTime, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<ScTime> data = scTimeMapper.selectScTimeList(scTime);
        return new PageInfo<>(data);
    }

    /**
     * 新增设置考勤时段
     * 
     * @param scTime 设置考勤时段
     * @return 结果
     */

    @Override
    public int insertScTime(ScTime scTime)
    {
        return scTimeMapper.insertScTime(scTime);
    }

    /**
     * 修改设置考勤时段
     * 
     * @param scTime 设置考勤时段
     * @return 结果
     */
    @Override
    public int updateScTime(ScTime scTime)
    {
        return scTimeMapper.updateScTime(scTime);
    }

    /**
     * 删除设置考勤时段对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteScTimeByIds(String[] ids)
    {
        return scTimeMapper.deleteScTimeByIds(ids);
    }

    /**
     * 删除设置考勤时段信息
     * 
     * @param id 设置考勤时段ID
     * @return 结果
     */
    @Override
    public int deleteScTimeById(Long id)
    {
        return scTimeMapper.deleteScTimeById(id);
    }
}
