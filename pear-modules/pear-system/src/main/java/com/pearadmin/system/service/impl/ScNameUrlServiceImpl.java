package com.pearadmin.system.service.impl;

import java.util.List;
import java.util.ArrayList;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pearadmin.system.mapper.ScNameUrlMapper;
import com.pearadmin.system.domain.ScNameUrl;
import com.pearadmin.system.service.IScNameUrlService;

/**
 * 考勤人员管理Service业务层处理
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Service
public class ScNameUrlServiceImpl implements IScNameUrlService 
{
    @Autowired
    private ScNameUrlMapper scNameUrlMapper;

    /**
     * 查询考勤人员管理
     * 
     * @param id 考勤人员管理ID
     * @return 考勤人员管理
     */
    @Override
    public ScNameUrl selectScNameUrlById(Long id)
    {
        return scNameUrlMapper.selectScNameUrlById(id);
    }

    /**
     * 查询考勤人员管理列表
     * 
     * @param scNameUrl 考勤人员管理
     * @return 考勤人员管理
     */
    @Override
    public List<ScNameUrl> selectScNameUrlList(ScNameUrl scNameUrl)
    {
        return scNameUrlMapper.selectScNameUrlList(scNameUrl);
    }

    /**
     * 查询考勤人员管理
     * @param scNameUrl 考勤人员管理
     * @param pageDomain
     * @return 考勤人员管理 分页集合
     * */
    @Override
    public PageInfo<ScNameUrl> selectScNameUrlPage(ScNameUrl scNameUrl, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<ScNameUrl> data = scNameUrlMapper.selectScNameUrlList(scNameUrl);
        return new PageInfo<>(data);
    }

    /**
     * 新增考勤人员管理
     * 
     * @param scNameUrl 考勤人员管理
     * @return 结果
     */

    @Override
    public int insertScNameUrl(ScNameUrl scNameUrl)
    {
        return scNameUrlMapper.insertScNameUrl(scNameUrl);
    }

    /**
     * 修改考勤人员管理
     * 
     * @param scNameUrl 考勤人员管理
     * @return 结果
     */
    @Override
    public int updateScNameUrl(ScNameUrl scNameUrl)
    {
        return scNameUrlMapper.updateScNameUrl(scNameUrl);
    }

    /**
     * 删除考勤人员管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteScNameUrlByIds(String[] ids)
    {
        return scNameUrlMapper.deleteScNameUrlByIds(ids);
    }

    /**
     * 删除考勤人员管理信息
     * 
     * @param id 考勤人员管理ID
     * @return 结果
     */
    @Override
    public int deleteScNameUrlById(Long id)
    {
        return scNameUrlMapper.deleteScNameUrlById(id);
    }
}
