package com.pearadmin.system.mapper;

import org.apache.ibatis.annotations.Mapper;
import java.util.List;
import com.pearadmin.system.domain.ScTime;

/**
 * 设置考勤时段Mapper接口
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Mapper
public interface ScTimeMapper 
{
    /**
     * 查询设置考勤时段
     * 
     * @param id 设置考勤时段ID
     * @return 设置考勤时段
     */
    public ScTime selectScTimeById(Long id);

    /**
     * 查询设置考勤时段列表
     * 
     * @param scTime 设置考勤时段
     * @return 设置考勤时段集合
     */
    List<ScTime> selectScTimeList(ScTime scTime);

    /**
     * 新增设置考勤时段
     * 
     * @param scTime 设置考勤时段
     * @return 结果
     */
    int insertScTime(ScTime scTime);

    /**
     * 修改设置考勤时段
     * 
     * @param scTime 设置考勤时段
     * @return 结果
     */
    int updateScTime(ScTime scTime);

    /**
     * 删除设置考勤时段
     * 
     * @param id 设置考勤时段ID
     * @return 结果
     */
    int deleteScTimeById(Long id);

    /**
     * 批量删除设置考勤时段
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    int deleteScTimeByIds(String[] ids);

}
