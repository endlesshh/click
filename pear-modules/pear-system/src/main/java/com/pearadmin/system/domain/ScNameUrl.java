package com.pearadmin.system.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 考勤人员管理对象 sc_name_url
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Data
public class ScNameUrl extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识 */
    private Long id;

    /** 姓名 */
    private String name;

    /** 照片 */
    private String url;

    /** 学号 */
    private String xh;

}
