package com.pearadmin.system.service.impl;

import java.util.List;
import java.util.ArrayList;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pearadmin.common.web.domain.request.PageDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pearadmin.system.mapper.ScLastPeopleTimeMapper;
import com.pearadmin.system.domain.ScLastPeopleTime;
import com.pearadmin.system.service.IScLastPeopleTimeService;

/**
 * 记录考勤信息Service业务层处理
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Service
public class ScLastPeopleTimeServiceImpl implements IScLastPeopleTimeService 
{
    @Autowired
    private ScLastPeopleTimeMapper scLastPeopleTimeMapper;

    /**
     * 查询记录考勤信息
     * 
     * @param id 记录考勤信息ID
     * @return 记录考勤信息
     */
    @Override
    public ScLastPeopleTime selectScLastPeopleTimeById(Long id)
    {
        return scLastPeopleTimeMapper.selectScLastPeopleTimeById(id);
    }

    /**
     * 查询记录考勤信息列表
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 记录考勤信息
     */
    @Override
    public List<ScLastPeopleTime> selectScLastPeopleTimeList(ScLastPeopleTime scLastPeopleTime)
    {
        return scLastPeopleTimeMapper.selectScLastPeopleTimeList(scLastPeopleTime);
    }

    /**
     * 查询记录考勤信息
     * @param scLastPeopleTime 记录考勤信息
     * @param pageDomain
     * @return 记录考勤信息 分页集合
     * */
    @Override
    public PageInfo<ScLastPeopleTime> selectScLastPeopleTimePage(ScLastPeopleTime scLastPeopleTime, PageDomain pageDomain)
    {
        PageHelper.startPage(pageDomain.getPage(),pageDomain.getLimit());
        List<ScLastPeopleTime> data = scLastPeopleTimeMapper.selectScLastPeopleTimeList(scLastPeopleTime);
        return new PageInfo<>(data);
    }

    /**
     * 新增记录考勤信息
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 结果
     */

    @Override
    public int insertScLastPeopleTime(ScLastPeopleTime scLastPeopleTime)
    {
        return scLastPeopleTimeMapper.insertScLastPeopleTime(scLastPeopleTime);
    }

    /**
     * 修改记录考勤信息
     * 
     * @param scLastPeopleTime 记录考勤信息
     * @return 结果
     */
    @Override
    public int updateScLastPeopleTime(ScLastPeopleTime scLastPeopleTime)
    {
        return scLastPeopleTimeMapper.updateScLastPeopleTime(scLastPeopleTime);
    }

    /**
     * 删除记录考勤信息对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteScLastPeopleTimeByIds(String[] ids)
    {
        return scLastPeopleTimeMapper.deleteScLastPeopleTimeByIds(ids);
    }

    /**
     * 删除记录考勤信息信息
     * 
     * @param id 记录考勤信息ID
     * @return 结果
     */
    @Override
    public int deleteScLastPeopleTimeById(Long id)
    {
        return scLastPeopleTimeMapper.deleteScLastPeopleTimeById(id);
    }
}
