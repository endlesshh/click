package com.pearadmin.system.controller;

import com.github.pagehelper.PageInfo;
import com.pearadmin.system.domain.ScTime;
import com.pearadmin.common.tools.string.Convert;
import com.pearadmin.common.web.base.BaseController;
import com.pearadmin.common.web.domain.request.PageDomain;
import com.pearadmin.common.web.domain.response.Result;
import com.pearadmin.common.web.domain.response.module.ResultTable;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.system.domain.SysUser;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import com.pearadmin.system.service.IScTimeService;

import java.time.LocalDateTime;

/**
 * 设置考勤时段Controller
 *
 * @author shiqiang
 * @date 2021-05-14
 */
@RestController
@RequestMapping("/system/sctime")
@Api(tags = {"考勤时段"})
public class ScTimeController extends BaseController
{
    private String prefix = "system/sctime";

    @Autowired
    private IScTimeService scTimeService;

    @GetMapping("/main")
    @PreAuthorize("hasPermission('/system/sctime/main','system:sctime:main')")
    public ModelAndView main()
    {
        return jumpPage(prefix + "/main");
    }

    /**
     * 查询设置考勤时段列表
     */
    @ResponseBody
    @GetMapping("/data")
    @PreAuthorize("hasPermission('/system/sctime/data','system:sctime:data')")
    public ResultTable list(@ModelAttribute ScTime scTime, PageDomain pageDomain)
    {
        PageInfo<ScTime> pageInfo = scTimeService.selectScTimePage(scTime,pageDomain);
        return pageTable(pageInfo.getList(),pageInfo.getTotal());
    }

    /**
     * 新增设置考勤时段
     */
    @GetMapping("/add")
    @PreAuthorize("hasPermission('/system/sctime/add','system:sctime:add')")
    public ModelAndView add()
    {
        return jumpPage(prefix + "/add");
    }

    /**
     * 新增保存设置考勤时段
     */
    @ResponseBody
    @PostMapping("/save")
    @PreAuthorize("hasPermission('/system/sctime/add','system:sctime:add')")
    public Result save(@RequestBody ScTime scTime)
    {
        SysUser sysUser = (SysUser)SecurityUtil.currentUserObj();
        scTime.setCreateTime(LocalDateTime.now());
        scTime.setCreateBy(sysUser.getUserId());
        scTime.setCreateName(sysUser.getUsername());
        return decide(scTimeService.insertScTime(scTime));
    }

    /**
     * 修改设置考勤时段
     */
    @GetMapping("/edit")
    @PreAuthorize("hasPermission('/system/sctime/edit','system:sctime:edit')")
    public ModelAndView edit(Long id, ModelMap mmap)
    {
        ScTime scTime = scTimeService.selectScTimeById(id);
        mmap.put("scTime", scTime);
        return jumpPage(prefix + "/edit");
    }

    /**
     * 修改保存设置考勤时段
     */
    @ResponseBody
    @PutMapping("/update")
    @PreAuthorize("hasPermission('/system/sctime/edit','system:sctime:edit')")
    public Result update(@RequestBody ScTime scTime)
    {
        SysUser sysUser = (SysUser)SecurityUtil.currentUserObj();
        scTime.setUpdateTime(LocalDateTime.now());
        scTime.setUpdateBy(sysUser.getUserId());
        scTime.setUpdateName(sysUser.getUsername());
        return decide(scTimeService.updateScTime(scTime));
    }

    /**
     * 删除设置考勤时段
     */
    @ResponseBody
    @DeleteMapping( "/batchRemove")
    @PreAuthorize("hasPermission('/system/sctime/remove','system:sctime:remove')")
    public Result batchRemove(String ids)
    {
        return decide(scTimeService.deleteScTimeByIds(Convert.toStrArray(ids)));
    }

    /**
     * 删除
     */
    @ResponseBody
    @DeleteMapping("/remove/{id}")
    @PreAuthorize("hasPermission('/system/sctime/remove','system:sctime:remove')")
    public Result remove(@PathVariable("id") Long id)
    {
        return decide(scTimeService.deleteScTimeById(id));
    }
}
