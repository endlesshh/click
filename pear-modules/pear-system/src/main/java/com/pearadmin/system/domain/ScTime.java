package com.pearadmin.system.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.pearadmin.common.web.base.BaseDomain;

/**
 * 设置考勤时段对象 sc_time
 * 
 * @author shiqiang
 * @date 2021-05-14
 */
@Data
public class ScTime extends BaseDomain
{
    private static final long serialVersionUID = 1L;

    /** 开始时间 */
    private String start;

    /** 唯一标识 */
    private Long id;

    /** 结束时间 */
    private String stop;

}
