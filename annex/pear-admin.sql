/*
Navicat MySQL Data Transfer

Source Server         : 182
Source Server Version : 50717
Source Host           : 192.168.0.182:3306
Source Database       : pear-admin

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2021-05-19 16:01:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `act_evt_log`
-- ----------------------------
DROP TABLE IF EXISTS `act_evt_log`;
CREATE TABLE `act_evt_log` (
  `LOG_NR_` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DATA_` longblob,
  `LOCK_OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`LOG_NR_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_evt_log
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ge_bytearray`
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_bytearray`;
CREATE TABLE `act_ge_bytearray` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BYTES_` longblob,
  `GENERATED_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_BYTEARR_DEPL` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ge_bytearray
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ge_property`
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_property`;
CREATE TABLE `act_ge_property` (
  `NAME_` varchar(64) COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  PRIMARY KEY (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ge_property
-- ----------------------------
INSERT INTO `act_ge_property` VALUES ('cfg.execution-related-entities-count', 'false', '1');
INSERT INTO `act_ge_property` VALUES ('next.dbid', '1', '1');
INSERT INTO `act_ge_property` VALUES ('schema.history', 'create(6.0.0.4)', '1');
INSERT INTO `act_ge_property` VALUES ('schema.version', '6.0.0.4', '1');

-- ----------------------------
-- Table structure for `act_hi_actinst`
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_actinst`;
CREATE TABLE `act_hi_actinst` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ACT_TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_ACT_INST_START` (`START_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_PROCINST` (`PROC_INST_ID_`,`ACT_ID_`),
  KEY `ACT_IDX_HI_ACT_INST_EXEC` (`EXECUTION_ID_`,`ACT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_actinst
-- ----------------------------

-- ----------------------------
-- Table structure for `act_hi_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_attachment`;
CREATE TABLE `act_hi_attachment` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `URL_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `CONTENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for `act_hi_comment`
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_comment`;
CREATE TABLE `act_hi_comment` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MESSAGE_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `FULL_MSG_` longblob,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `act_hi_detail`
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_detail`;
CREATE TABLE `act_hi_detail` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_DETAIL_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_ACT_INST` (`ACT_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_TIME` (`TIME_`),
  KEY `ACT_IDX_HI_DETAIL_NAME` (`NAME_`),
  KEY `ACT_IDX_HI_DETAIL_TASK_ID` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_detail
-- ----------------------------

-- ----------------------------
-- Table structure for `act_hi_identitylink`
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_identitylink`;
CREATE TABLE `act_hi_identitylink` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_TASK` (`TASK_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_identitylink
-- ----------------------------

-- ----------------------------
-- Table structure for `act_hi_procinst`
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_procinst`;
CREATE TABLE `act_hi_procinst` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `START_USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `END_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `PROC_INST_ID_` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PRO_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_PRO_I_BUSKEY` (`BUSINESS_KEY_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_procinst
-- ----------------------------

-- ----------------------------
-- Table structure for `act_hi_taskinst`
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_taskinst`;
CREATE TABLE `act_hi_taskinst` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) DEFAULT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int(11) DEFAULT NULL,
  `DUE_DATE_` datetime(3) DEFAULT NULL,
  `FORM_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_TASK_INST_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_taskinst
-- ----------------------------

-- ----------------------------
-- Table structure for `act_hi_varinst`
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_varinst`;
CREATE TABLE `act_hi_varinst` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_PROCVAR_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PROCVAR_NAME_TYPE` (`NAME_`,`VAR_TYPE_`),
  KEY `ACT_IDX_HI_PROCVAR_TASK_ID` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_varinst
-- ----------------------------

-- ----------------------------
-- Table structure for `act_id_group`
-- ----------------------------
DROP TABLE IF EXISTS `act_id_group`;
CREATE TABLE `act_id_group` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_id_group
-- ----------------------------

-- ----------------------------
-- Table structure for `act_id_info`
-- ----------------------------
DROP TABLE IF EXISTS `act_id_info`;
CREATE TABLE `act_id_info` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `VALUE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PASSWORD_` longblob,
  `PARENT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_id_info
-- ----------------------------

-- ----------------------------
-- Table structure for `act_id_membership`
-- ----------------------------
DROP TABLE IF EXISTS `act_id_membership`;
CREATE TABLE `act_id_membership` (
  `USER_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`,`GROUP_ID_`),
  KEY `ACT_FK_MEMB_GROUP` (`GROUP_ID_`),
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`),
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_id_membership
-- ----------------------------

-- ----------------------------
-- Table structure for `act_id_user`
-- ----------------------------
DROP TABLE IF EXISTS `act_id_user`;
CREATE TABLE `act_id_user` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `FIRST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LAST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PWD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PICTURE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_id_user
-- ----------------------------

-- ----------------------------
-- Table structure for `act_procdef_info`
-- ----------------------------
DROP TABLE IF EXISTS `act_procdef_info`;
CREATE TABLE `act_procdef_info` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `INFO_JSON_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_INFO_PROCDEF` (`PROC_DEF_ID_`),
  KEY `ACT_IDX_INFO_PROCDEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_INFO_JSON_BA` (`INFO_JSON_ID_`),
  CONSTRAINT `ACT_FK_INFO_JSON_BA` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_INFO_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_procdef_info
-- ----------------------------

-- ----------------------------
-- Table structure for `act_re_deployment`
-- ----------------------------
DROP TABLE IF EXISTS `act_re_deployment`;
CREATE TABLE `act_re_deployment` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  `ENGINE_VERSION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_re_deployment
-- ----------------------------

-- ----------------------------
-- Table structure for `act_re_model`
-- ----------------------------
DROP TABLE IF EXISTS `act_re_model`;
CREATE TABLE `act_re_model` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `VERSION_` int(11) DEFAULT NULL,
  `META_INFO_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_MODEL_SOURCE` (`EDITOR_SOURCE_VALUE_ID_`),
  KEY `ACT_FK_MODEL_SOURCE_EXTRA` (`EDITOR_SOURCE_EXTRA_VALUE_ID_`),
  KEY `ACT_FK_MODEL_DEPLOYMENT` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`),
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_re_model
-- ----------------------------

-- ----------------------------
-- Table structure for `act_re_procdef`
-- ----------------------------
DROP TABLE IF EXISTS `act_re_procdef`;
CREATE TABLE `act_re_procdef` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint(4) DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `ENGINE_VERSION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_PROCDEF` (`KEY_`,`VERSION_`,`TENANT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_re_procdef
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_deadletter_job`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_deadletter_job`;
CREATE TABLE `act_ru_deadletter_job` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_DEADLETTER_JOB_EXECUTION` (`EXECUTION_ID_`),
  KEY `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`),
  KEY `ACT_FK_DEADLETTER_JOB_PROC_DEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_DEADLETTER_JOB_EXCEPTION` (`EXCEPTION_STACK_ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_DEADLETTER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_deadletter_job
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_event_subscr`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_event_subscr`;
CREATE TABLE `act_ru_event_subscr` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `EVENT_TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `EVENT_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACTIVITY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CONFIGURATION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_EVENT_SUBSCR_CONFIG_` (`CONFIGURATION_`),
  KEY `ACT_FK_EVENT_EXEC` (`EXECUTION_ID_`),
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_event_subscr
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_execution`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_execution`;
CREATE TABLE `act_ru_execution` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ROOT_PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IS_ACTIVE_` tinyint(4) DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(4) DEFAULT NULL,
  `IS_SCOPE_` tinyint(4) DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint(4) DEFAULT NULL,
  `IS_MI_ROOT_` tinyint(4) DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL,
  `CACHED_ENT_STATE_` int(11) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) DEFAULT NULL,
  `START_USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_COUNT_ENABLED_` tinyint(4) DEFAULT NULL,
  `EVT_SUBSCR_COUNT_` int(11) DEFAULT NULL,
  `TASK_COUNT_` int(11) DEFAULT NULL,
  `JOB_COUNT_` int(11) DEFAULT NULL,
  `TIMER_JOB_COUNT_` int(11) DEFAULT NULL,
  `SUSP_JOB_COUNT_` int(11) DEFAULT NULL,
  `DEADLETTER_JOB_COUNT_` int(11) DEFAULT NULL,
  `VAR_COUNT_` int(11) DEFAULT NULL,
  `ID_LINK_COUNT_` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_EXEC_BUSKEY` (`BUSINESS_KEY_`),
  KEY `ACT_IDC_EXEC_ROOT` (`ROOT_PROC_INST_ID_`),
  KEY `ACT_FK_EXE_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_EXE_PARENT` (`PARENT_ID_`),
  KEY `ACT_FK_EXE_SUPER` (`SUPER_EXEC_`),
  KEY `ACT_FK_EXE_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE,
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_execution
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_identitylink`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_identitylink`;
CREATE TABLE `act_ru_identitylink` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `GROUP_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_IDENT_LNK_GROUP` (`GROUP_ID_`),
  KEY `ACT_IDX_ATHRZ_PROCEDEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_TSKASS_TASK` (`TASK_ID_`),
  KEY `ACT_FK_IDL_PROCINST` (`PROC_INST_ID_`),
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `act_ru_task` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_identitylink
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_job`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_job`;
CREATE TABLE `act_ru_job` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int(11) DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_JOB_EXECUTION` (`EXECUTION_ID_`),
  KEY `ACT_FK_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`),
  KEY `ACT_FK_JOB_PROC_DEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_JOB_EXCEPTION` (`EXCEPTION_STACK_ID_`),
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_job
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_suspended_job`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_suspended_job`;
CREATE TABLE `act_ru_suspended_job` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int(11) DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_SUSPENDED_JOB_EXECUTION` (`EXECUTION_ID_`),
  KEY `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`),
  KEY `ACT_FK_SUSPENDED_JOB_PROC_DEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_SUSPENDED_JOB_EXCEPTION` (`EXCEPTION_STACK_ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_SUSPENDED_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_suspended_job
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_task`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_task`;
CREATE TABLE `act_ru_task` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DELEGATION_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int(11) DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `FORM_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CLAIM_TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_TASK_CREATE` (`CREATE_TIME_`),
  KEY `ACT_FK_TASK_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_TASK_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_TASK_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_task
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_timer_job`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_timer_job`;
CREATE TABLE `act_ru_timer_job` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int(11) DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_TIMER_JOB_EXECUTION` (`EXECUTION_ID_`),
  KEY `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` (`PROCESS_INSTANCE_ID_`),
  KEY `ACT_FK_TIMER_JOB_PROC_DEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_TIMER_JOB_EXCEPTION` (`EXCEPTION_STACK_ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_EXECUTION` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_PROCESS_INSTANCE` FOREIGN KEY (`PROCESS_INSTANCE_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TIMER_JOB_PROC_DEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_timer_job
-- ----------------------------

-- ----------------------------
-- Table structure for `act_ru_variable`
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_variable`;
CREATE TABLE `act_ru_variable` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_VARIABLE_TASK_ID` (`TASK_ID_`),
  KEY `ACT_FK_VAR_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_VAR_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_VAR_BYTEARRAY` (`BYTEARRAY_ID_`),
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_variable
-- ----------------------------

-- ----------------------------
-- Table structure for `gen_table`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` varchar(20) NOT NULL COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `parent_menu_id` varchar(40) DEFAULT NULL COMMENT '父级菜单',
  `parent_menu_name` varchar(255) DEFAULT NULL COMMENT '父级菜单名称',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES ('1328525218309734400', 'sys_user', 'App用户', null, null, 'SysUser', 'crud', 'com.pearadmin.system', 'system', 'user', 'App用户', 'Jmys', '1', '/pear-system', 'null', '', '2020-11-17 10:24:33', '', '2021-04-02 16:07:18', '', '1', '系统管理');
INSERT INTO `gen_table` VALUES ('1370410322996756480', 'sys_notice', '站内消息', null, null, 'SysNotice', 'crud', 'com.pearadmin.system', 'system', 'notice', 'notice', 'jmys', '1', 'D:\\openSource\\Pear-Admin-Boot\\pear-modules\\pear-system', 'null', '', '2021-03-12 16:24:16', '', '2021-03-30 18:04:17', '生成', '1', '系统管理');
INSERT INTO `gen_table` VALUES ('1393091143238942720', 'sc_last_people_time', '考勤记录', null, null, 'ScLastPeopleTime', 'crud', 'com.pearadmin.system', 'system', 'lastPeopleTime', '记录考勤信息', 'shiqiang', '0', '/', 'null', '', '2021-05-14 14:29:45', '', '2021-05-14 15:21:49', '', '', '');
INSERT INTO `gen_table` VALUES ('1393091143238942721', 'sc_name_url', '考勤人员信息', null, null, 'ScNameUrl', 'crud', 'com.pearadmin.system', 'system', 'nameUrl', '考勤人员管理', 'shiqiang', '0', '/', 'null', '', '2021-05-14 14:29:45', '', '2021-05-14 15:26:52', '', '', '');
INSERT INTO `gen_table` VALUES ('1393091143238942722', 'sc_time', '考勤时段', null, null, 'ScTime', 'crud', 'com.pearadmin.system', 'system', 'sctime', '设置考勤时段', 'shiqiang', '0', '/', 'null', '', '2021-05-14 14:29:45', '', '2021-05-14 15:20:14', '', '', '');
INSERT INTO `gen_table` VALUES ('1393091143238942723', 'sc_user', '', null, null, 'ScUser', 'crud', 'com.pearadmin.system', 'system', 'user', null, 'jmys', '0', '/', null, '', '2021-05-14 14:29:45', '', null, null, null, null);

-- ----------------------------
-- Table structure for `gen_table_column`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` varchar(20) NOT NULL COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES ('1328525219052126208', '1328525218309734400', 'user_id', '编号', 'char(19)', 'String', 'userId', '1', '0', null, '1', null, null, null, 'EQ', 'input', 'input', '1', '', '2020-11-17 10:24:33', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525219400253440', '1328525218309734400', 'username', '账户', 'char(20)', 'String', 'username', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', 'input', '2', '', '2020-11-17 10:24:33', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525219786129408', '1328525218309734400', 'password', '密码', 'char(60)', 'String', 'password', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '3', '', '2020-11-17 10:24:33', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525220104896512', '1328525218309734400', 'salt', '姓名', 'char(10)', 'String', 'salt', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '4', '', '2020-11-17 10:24:33', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525220423663616', '1328525218309734400', 'status', '状态', 'char(1)', 'String', 'status', '0', '0', null, '1', '1', '1', '1', 'EQ', 'radio', 'input', '5', '', '2020-11-17 10:24:33', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525220734042112', '1328525218309734400', 'real_name', '姓名', 'char(8)', 'String', 'realName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', 'input', '6', '', '2020-11-17 10:24:33', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525221044420608', '1328525218309734400', 'email', '邮箱', 'char(20)', 'String', 'email', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '7', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525221363187712', '1328525218309734400', 'avatar', '头像', 'varchar(30)', 'String', 'avatar', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', 'input', '8', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525221677760512', '1328525218309734400', 'sex', '性别', 'char(1)', 'String', 'sex', '0', '0', null, '1', '1', '1', '1', 'EQ', 'select', 'user_sex', '9', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525222000721920', '1328525218309734400', 'phone', '电话', 'char(11)', 'String', 'phone', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '10', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525222415958016', '1328525218309734400', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', 'input', '11', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525222764085248', '1328525218309734400', 'create_by', '创建人', 'char(1)', 'String', 'createBy', '0', '0', null, '1', null, null, null, 'EQ', 'input', 'input', '12', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525223091240960', '1328525218309734400', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, 'EQ', 'datetime', 'input', '13', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525223552614400', '1328525218309734400', 'update_by', '修改人', 'char(1)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, 'EQ', 'input', 'input', '14', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525223896547328', '1328525218309734400', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', null, '1', '1', '1', null, 'EQ', 'input', 'input', '15', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525224206925824', '1328525218309734400', 'enable', '是否启用', 'char(1)', 'String', 'enable', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '16', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525224542470144', '1328525218309734400', 'login', '是否登录', 'char(1)', 'String', 'login', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '17', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1328525224861237248', '1328525218309734400', 'dept_id', '部门编号', 'char(19)', 'String', 'deptId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '18', '', '2020-11-17 10:24:34', null, '2021-04-02 16:07:18');
INSERT INTO `gen_table_column` VALUES ('1370410323613319168', '1370410322996756480', 'id', '编号', 'char(20)', 'String', 'id', '1', '0', null, '1', null, null, null, 'EQ', 'input', 'input', '1', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410323856588800', '1370410322996756480', 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', 'input', '2', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410324095664128', '1370410322996756480', 'content', '内容', 'text', 'String', 'content', '0', '0', '1', '1', '1', '1', null, 'EQ', 'input', 'input', '3', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410324317962240', '1370410322996756480', 'sender', '发送人', 'char(20)', 'String', 'sender', '0', '0', '1', '1', null, '1', null, 'EQ', 'select', 'input', '4', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410324557037568', '1370410322996756480', 'accept', '接收者', 'char(20)', 'String', 'accept', '0', '0', '1', '1', '1', '1', null, 'EQ', 'input', 'input', '5', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410324766752768', '1370410322996756480', 'type', '类型', 'char(10)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'sys_notice_type', '6', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410325018411008', '1370410322996756480', 'create_by', '创建人', 'char(20)', 'String', 'createBy', '0', '0', null, '1', null, null, null, 'EQ', 'input', 'input', '7', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410325240709120', '1370410322996756480', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', 'input', '8', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410325471395840', '1370410322996756480', 'update_by', '修改人', 'char(20)', 'String', 'updateBy', '0', '0', null, '1', null, null, null, 'EQ', 'input', 'input', '9', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410325702082560', '1370410322996756480', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', 'input', '10', '', '2021-03-12 16:24:16', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1370410325928574976', '1370410322996756480', 'remark', '备注', 'varchar(255)', 'String', 'remark', '0', '0', null, '1', null, null, null, 'EQ', 'input', 'input', '11', '', '2021-03-12 16:24:17', null, '2021-03-30 18:04:17');
INSERT INTO `gen_table_column` VALUES ('1393091143364771840', '1393091143238942720', 'id', '唯一标识', 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', 'input', '1', '', '2021-05-14 14:29:45', null, '2021-05-14 15:21:49');
INSERT INTO `gen_table_column` VALUES ('1393091143394131968', '1393091143238942720', 'time', '日期', 'varchar(100)', 'String', 'time', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '2', '', '2021-05-14 14:29:45', null, '2021-05-14 15:21:49');
INSERT INTO `gen_table_column` VALUES ('1393091143410909184', '1393091143238942720', 'start', '考勤开始时间', 'varchar(100)', 'String', 'start', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '3', '', '2021-05-14 14:29:45', null, '2021-05-14 15:21:49');
INSERT INTO `gen_table_column` VALUES ('1393091143427686400', '1393091143238942720', 'stop', '考勤结束时间', 'varchar(100)', 'String', 'stop', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '4', '', '2021-05-14 14:29:45', null, '2021-05-14 15:21:49');
INSERT INTO `gen_table_column` VALUES ('1393091143440269312', '1393091143238942720', 'people1', '应到人数', 'varchar(100)', 'String', 'people1', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '5', '', '2021-05-14 14:29:45', null, '2021-05-14 15:21:49');
INSERT INTO `gen_table_column` VALUES ('1393091143452852224', '1393091143238942720', 'people2', '实到人数', 'varchar(100)', 'String', 'people2', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '6', '', '2021-05-14 14:29:45', null, '2021-05-14 15:21:49');
INSERT INTO `gen_table_column` VALUES ('1393091143465435136', '1393091143238942720', 'text', '考勤人员列表', 'varchar(100)', 'String', 'text', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '7', '', '2021-05-14 14:29:45', null, '2021-05-14 15:21:49');
INSERT INTO `gen_table_column` VALUES ('1393091143503183872', '1393091143238942721', 'id', '唯一标识', 'int(11)', 'Long', 'id', '0', '0', null, null, null, null, null, 'EQ', 'input', 'input', '1', '', '2021-05-14 14:29:45', null, '2021-05-14 15:26:52');
INSERT INTO `gen_table_column` VALUES ('1393091143515766784', '1393091143238942721', 'name', '姓名', 'varchar(50)', 'String', 'name', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', 'input', '2', '', '2021-05-14 14:29:45', null, '2021-05-14 15:26:52');
INSERT INTO `gen_table_column` VALUES ('1393091143528349696', '1393091143238942721', 'url', '照片', 'varchar(500)', 'String', 'url', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '3', '', '2021-05-14 14:29:45', null, '2021-05-14 15:26:52');
INSERT INTO `gen_table_column` VALUES ('1393091143540932608', '1393091143238942721', 'xh', '学号', 'varchar(50)', 'String', 'xh', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '4', '', '2021-05-14 14:29:45', null, '2021-05-14 15:26:52');
INSERT INTO `gen_table_column` VALUES ('1393091143578681344', '1393091143238942722', 'start', '开始时间', 'varchar(100)', 'String', 'start', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '1', '', '2021-05-14 14:29:45', null, '2021-05-14 15:20:14');
INSERT INTO `gen_table_column` VALUES ('1393091143591264256', '1393091143238942722', 'id', '唯一标识', 'int(11)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', 'input', '2', '', '2021-05-14 14:29:45', null, '2021-05-14 15:20:14');
INSERT INTO `gen_table_column` VALUES ('1393091143608041472', '1393091143238942722', 'stop', '结束时间', 'varchar(100)', 'String', 'stop', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', 'input', '3', '', '2021-05-14 14:29:45', null, '2021-05-14 15:20:14');
INSERT INTO `gen_table_column` VALUES ('1393091143649984512', '1393091143238942723', 'uid', null, 'int(20)', 'Long', 'uid', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', '', '2021-05-14 14:29:45', '', null);
INSERT INTO `gen_table_column` VALUES ('1393091143662567424', '1393091143238942723', 'username', null, 'varchar(20)', 'String', 'username', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '2', '', '2021-05-14 14:29:45', '', null);
INSERT INTO `gen_table_column` VALUES ('1393091143675150336', '1393091143238942723', 'password', null, 'varchar(40)', 'String', 'password', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', '', '2021-05-14 14:29:45', '', null);
INSERT INTO `gen_table_column` VALUES ('1393091143691927552', '1393091143238942723', 'admin', null, 'varchar(10)', 'String', 'admin', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', '', '2021-05-14 14:29:45', '', null);

-- ----------------------------
-- Table structure for `schedule_calendars`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_calendars`;
CREATE TABLE `schedule_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_cron_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_cron_triggers`;
CREATE TABLE `schedule_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `schedule_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `schedule_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_cron_triggers
-- ----------------------------
INSERT INTO `schedule_cron_triggers` VALUES ('PearScheduler', 'Pear_1361202980476420096', 'DEFAULT', '*/5 * * * * ?', 'Asia/Shanghai');
INSERT INTO `schedule_cron_triggers` VALUES ('PearScheduler', 'Pear_1361243793570922496', 'DEFAULT', '*/5 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for `schedule_fired_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_fired_triggers`;
CREATE TABLE `schedule_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(20) NOT NULL,
  `SCHED_TIME` bigint(20) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`) USING BTREE,
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`) USING BTREE,
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`) USING BTREE,
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_group`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_group`;
CREATE TABLE `schedule_group` (
  `group_id` char(19) NOT NULL COMMENT '注释',
  `group_name` varchar(255) DEFAULT NULL COMMENT '组名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` char(19) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` char(19) DEFAULT NULL COMMENT '修改人',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`group_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_group
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_job`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` char(19) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `job_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`job_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('1361202980476420096', 'commonTask', '123456', '*/5 * * * * ?', '1', '测试任务', '2021-02-15 14:37:35', '测试任务');
INSERT INTO `schedule_job` VALUES ('1361243793570922496', 'exceptionTask', '123456', '*/5 * * * * ?', '1', '异常任务', '2021-02-15 17:19:46', '异常任务');

-- ----------------------------
-- Table structure for `schedule_job_details`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_details`;
CREATE TABLE `schedule_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`) USING BTREE,
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_job_details
-- ----------------------------
INSERT INTO `schedule_job_details` VALUES ('PearScheduler', 'Pear_1361202980476420096', 'DEFAULT', null, 'com.pearadmin.schedule.handler.ScheduleContext', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B455973720029636F6D2E7065617261646D696E2E7363686564756C652E646F6D61696E2E5363686564756C654A6F62C4760C6A9C2FA04B0200094C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400194C6A6176612F74696D652F4C6F63616C4461746554696D653B4C000E63726F6E45787072657373696F6E71007E00094C000767726F7570496471007E00094C00056A6F62496471007E00094C00076A6F624E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C000673746174757371007E0009787074000A636F6D6D6F6E5461736B7372000D6A6176612E74696D652E536572955D84BA1B2248B20C00007870770E05000007E5020F0E252307DB4CF87874000D2A2F35202A202A202A202A203F707400133133363132303239383034373634323030393674000CE6B58BE8AF95E4BBBBE58AA174000631323334353674000CE6B58BE8AF95E4BBBBE58AA1740001307800);
INSERT INTO `schedule_job_details` VALUES ('PearScheduler', 'Pear_1361243793570922496', 'DEFAULT', null, 'com.pearadmin.schedule.handler.ScheduleContext', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B455973720029636F6D2E7065617261646D696E2E7363686564756C652E646F6D61696E2E5363686564756C654A6F62C4760C6A9C2FA04B0200094C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400194C6A6176612F74696D652F4C6F63616C4461746554696D653B4C000E63726F6E45787072657373696F6E71007E00094C000767726F7570496471007E00094C00056A6F62496471007E00094C00076A6F624E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C000673746174757371007E0009787074000D657863657074696F6E5461736B7372000D6A6176612E74696D652E536572955D84BA1B2248B20C00007870770E05000007E5020F11132D2B9938447874000D2A2F35202A202A202A202A203F707400133133363132343337393335373039323234393674000CE5BC82E5B8B8E4BBBBE58AA174000331323374000CE5BC82E5B8B8E4BBBBE58AA1740001307800);

-- ----------------------------
-- Table structure for `schedule_locks`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_locks`;
CREATE TABLE `schedule_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_locks
-- ----------------------------
INSERT INTO `schedule_locks` VALUES ('PearScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for `schedule_log`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_log`;
CREATE TABLE `schedule_log` (
  `log_id` char(19) NOT NULL COMMENT '任务日志id',
  `job_id` char(19) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态    0：成功    1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`) USING BTREE,
  KEY `job_id` (`job_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_log
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_paused_trigger_grps`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_paused_trigger_grps`;
CREATE TABLE `schedule_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_scheduler_state`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_scheduler_state`;
CREATE TABLE `schedule_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(20) NOT NULL,
  `CHECKIN_INTERVAL` bigint(20) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_simple_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_simple_triggers`;
CREATE TABLE `schedule_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(20) NOT NULL,
  `REPEAT_INTERVAL` bigint(20) NOT NULL,
  `TIMES_TRIGGERED` bigint(20) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `schedule_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `schedule_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_simprop_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_simprop_triggers`;
CREATE TABLE `schedule_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  CONSTRAINT `schedule_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `schedule_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for `schedule_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `schedule_triggers`;
CREATE TABLE `schedule_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(20) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(20) NOT NULL,
  `END_TIME` bigint(20) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(6) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`) USING BTREE,
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`) USING BTREE,
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`) USING BTREE,
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE,
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE,
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`) USING BTREE,
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`) USING BTREE,
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`) USING BTREE,
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`) USING BTREE,
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`) USING BTREE,
  CONSTRAINT `schedule_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `schedule_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of schedule_triggers
-- ----------------------------
INSERT INTO `schedule_triggers` VALUES ('PearScheduler', 'Pear_1361202980476420096', 'DEFAULT', 'Pear_1361202980476420096', 'DEFAULT', null, '1614173955000', '1614173950000', '5', 'PAUSED', 'CRON', '1613371055000', '0', null, '2', '');
INSERT INTO `schedule_triggers` VALUES ('PearScheduler', 'Pear_1361243793570922496', 'DEFAULT', 'Pear_1361243793570922496', 'DEFAULT', null, '1614173955000', '1614173950000', '5', 'PAUSED', 'CRON', '1613380785000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B455973720029636F6D2E7065617261646D696E2E7363686564756C652E646F6D61696E2E5363686564756C654A6F62C4760C6A9C2FA04B0200094C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400194C6A6176612F74696D652F4C6F63616C4461746554696D653B4C000E63726F6E45787072657373696F6E71007E00094C000767726F7570496471007E00094C00056A6F62496471007E00094C00076A6F624E616D6571007E00094C0006706172616D7371007E00094C000672656D61726B71007E00094C000673746174757371007E0009787074000D657863657074696F6E5461736B7074000D2A2F35202A202A202A202A203F707400133133363132343337393335373039323234393674000CE5BC82E5B8B8E4BBBBE58AA174000631323334353674000CE5BC82E5B8B8E4BBBBE58AA1740001307800);

-- ----------------------------
-- Table structure for `sc_last_people_time`
-- ----------------------------
DROP TABLE IF EXISTS `sc_last_people_time`;
CREATE TABLE `sc_last_people_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` varchar(100) DEFAULT NULL,
  `start` varchar(100) DEFAULT NULL,
  `stop` varchar(100) DEFAULT NULL,
  `people1` varchar(100) DEFAULT NULL,
  `people2` varchar(100) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sc_last_people_time
-- ----------------------------
INSERT INTO `sc_last_people_time` VALUES ('2', '2021-05-12', '15:00', '15:00', '1', '1', '张三#李四#王五#赵六#');
INSERT INTO `sc_last_people_time` VALUES ('3', '2021-05-12', '15:00', '15:00', '1', '1', '张三#李四#王五#赵六#');
INSERT INTO `sc_last_people_time` VALUES ('4', '2021-05-12', '15:00', '15:00', '1', '1', '张三#李四#王五#赵六#');
INSERT INTO `sc_last_people_time` VALUES ('5', '2021-05-12', '15:00', '15:00', '1', '1', '张三#李四#王五#赵六#');
INSERT INTO `sc_last_people_time` VALUES ('6', '2021-05-12', '15:00', '15:00', '1', '1', '张三#李四#王五#赵六#');
INSERT INTO `sc_last_people_time` VALUES ('7', '2021-05-12', '15:00', '15:00', '1', '1', '张三#李四#王五#赵六#');
INSERT INTO `sc_last_people_time` VALUES ('8', '2021-05-12', '15:00', '15:00', '1', '1', '张三#李四#王五#赵六#');
INSERT INTO `sc_last_people_time` VALUES ('9', '2021-05-12', '15:00', '15:00', '1', '1', '张三#李四#王五#赵六#');
INSERT INTO `sc_last_people_time` VALUES ('17', '2021-05-13', '13:00', '20:00', '4', '1', 'qq#');
INSERT INTO `sc_last_people_time` VALUES ('18', '2021-05-14', '07:00', '12:00', '3', '1', 'e#');
INSERT INTO `sc_last_people_time` VALUES ('45', '2021-05-17', '13:00', '20:00', '2', '1', '石强#');
INSERT INTO `sc_last_people_time` VALUES ('47', '2021-05-19', '13:00', '20:00', '3', '2', '奥巴马1#啊啊啊#');
INSERT INTO `sc_last_people_time` VALUES ('48', '2021-05-19', '11:00', '20:00', '4', '1', '啊啊啊#');

-- ----------------------------
-- Table structure for `sc_name_url`
-- ----------------------------
DROP TABLE IF EXISTS `sc_name_url`;
CREATE TABLE `sc_name_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `url` varchar(500) DEFAULT NULL,
  `xh` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sc_name_url
-- ----------------------------
INSERT INTO `sc_name_url` VALUES ('1', '石强', '1394199551404933120', '11111111111');
INSERT INTO `sc_name_url` VALUES ('2', '奥巴马', '1394188889798737920', '22222222222');
INSERT INTO `sc_name_url` VALUES ('3', '奥巴马1', '1394913551281291264', '55555555555');

-- ----------------------------
-- Table structure for `sc_time`
-- ----------------------------
DROP TABLE IF EXISTS `sc_time`;
CREATE TABLE `sc_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` varchar(100) DEFAULT NULL,
  `stop` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sc_time
-- ----------------------------
INSERT INTO `sc_time` VALUES ('1', '19:00', '20:00');
INSERT INTO `sc_time` VALUES ('2', '13:00', '20:00');
INSERT INTO `sc_time` VALUES ('5', '19:00', '20:00');
INSERT INTO `sc_time` VALUES ('6', '19:00', '21:00');

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` varchar(19) NOT NULL COMMENT '配置标识',
  `config_name` char(20) DEFAULT NULL COMMENT '配置名称',
  `config_code` char(20) DEFAULT NULL COMMENT '配置标识',
  `config_value` varchar(255) DEFAULT NULL COMMENT '配置值',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` char(20) DEFAULT NULL COMMENT '创建人',
  `update_by` char(1) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `config_type` char(10) DEFAULT NULL COMMENT '配置类型',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '1', 'oss_point', 'oss-cn-beijing.aliyuncs.com', null, null, null, null, null, null);
INSERT INTO `sys_config` VALUES ('1307313917164257280', '网站描述', 'system_desc', '网站描述', '2020-11-08 19:19:32', null, null, null, '网站描述', 'custom');
INSERT INTO `sys_config` VALUES ('1309118169381601280', '网站数据', 'system_meta', '网站数据', '2020-11-03 19:20:48', null, null, null, '网站数据', 'custom');
INSERT INTO `sys_config` VALUES ('1356140265433202688', '系统配置', 'main_from', '854085467@qq.com', null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('1356140265865216000', '系统配置', 'main_user', '854085467', null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('1356140266297229312', '系统配置', 'main_pass', '123456', null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('1356140266754408448', '系统配置', 'main_port', '456', null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('1356140267211587584', '系统配置', 'main_host', 'smtp.qq.com', null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('1356178612746715136', '系统配置', 'oss_path', 'D://upload', null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('1356178613115813888', '系统配置', 'oss_type', 'local', null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('1370975131278508032', '上传方式', 'upload_kind', null, null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('1370975131630829568', '上传路径', 'upload_path', null, null, null, null, null, null, 'system');
INSERT INTO `sys_config` VALUES ('2', '2', 'oss_key', 'LTAI4G8ZDXDU6DiibSVd8G2b', null, null, null, null, null, null);
INSERT INTO `sys_config` VALUES ('3', '3', 'oss_secret', '9apyAWE7Xfu7NP5jgFHFdXeyPa28jL', null, null, null, null, null, null);
INSERT INTO `sys_config` VALUES ('4', '4', 'oss_bucket', 'pearadmin-bbs', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` varchar(20) NOT NULL COMMENT '部门名称',
  `parent_id` varchar(20) DEFAULT NULL COMMENT '父级编号',
  `dept_name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `leader` varchar(50) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(20) DEFAULT NULL COMMENT '联系方式',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` varchar(1) DEFAULT NULL COMMENT '部门状态',
  `create_by` varchar(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(20) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` text COMMENT '备注',
  `address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '0', '济南总公司', '1', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('10', '8', '设计部', '3', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('1316361008259792896', '1316360459930042368', '软件部', '1', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('1316361192645591040', '1316360459930042368', '市场部', '1', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('1377824449830584320', '3', '财务部', '1', '就眠仪式', '15553726531', '854085467@qq.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('1377825171905183744', '8', '财务部', '1', '就眠仪式', '15553726531', '854085467@qq.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('3', '1', '杭州分公司', '1', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '浙江杭州');
INSERT INTO `sys_dept` VALUES ('4', '2', '软件部', '2', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('5', '2', '市场部', '2', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('6', '3', '软件部', '3', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '浙江杭州');
INSERT INTO `sys_dept` VALUES ('7', '3', '设计部', '4', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('8', '1', '深圳分公司', '3', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');
INSERT INTO `sys_dept` VALUES ('9', '8', '软件部', '3', '就眠仪式', '15553726531', 'pearadmin@gmail.com', '0', null, null, null, null, null, '山东济南');

-- ----------------------------
-- Table structure for `sys_dict_data`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `data_id` char(19) NOT NULL COMMENT '标识',
  `data_label` char(19) DEFAULT NULL COMMENT '字典标签',
  `data_value` char(20) DEFAULT NULL COMMENT '字典值',
  `type_code` char(20) DEFAULT NULL COMMENT '所属类型',
  `is_default` char(1) DEFAULT NULL COMMENT '是否默认',
  `update_by` char(19) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `enable` char(1) DEFAULT NULL COMMENT '是否启用',
  PRIMARY KEY (`data_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '男', '0', 'system_user_sex', '0', null, null, null, null, '描述', '1');
INSERT INTO `sys_dict_data` VALUES ('1302833449496739840', '字典名称', '字典值', 'dict_code', '1', null, null, null, null, 'aw', '0');
INSERT INTO `sys_dict_data` VALUES ('1317401149287956480', '男', 'boy', 'user_sex', null, null, null, null, null, '男 : body', '0');
INSERT INTO `sys_dict_data` VALUES ('1317402976670711808', '女', 'girl', 'user_sex', null, null, null, null, null, '女 : girl', '0');
INSERT INTO `sys_dict_data` VALUES ('1370411072367886336', '公告', 'public', 'sys_notice_type', null, null, null, null, null, '公告', '0');
INSERT INTO `sys_dict_data` VALUES ('1370411179544936448', '私信', 'private', 'sys_notice_type', null, null, null, null, null, '私信', '0');
INSERT INTO `sys_dict_data` VALUES ('1387076909438861312', '短信', 'smmm', 'sys_notice_type', null, null, null, null, null, null, '0');
INSERT INTO `sys_dict_data` VALUES ('2', '女', '1', 'system_user_sex', '1', null, null, null, null, '描述', '0');
INSERT INTO `sys_dict_data` VALUES ('447572898392182784', 'awd', 'awd', 'dict_code', '1', null, null, null, null, 'awd', '0');

-- ----------------------------
-- Table structure for `sys_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `id` char(19) NOT NULL COMMENT '标识',
  `type_name` varchar(255) DEFAULT NULL COMMENT '字典类型名称',
  `type_code` varchar(255) DEFAULT NULL COMMENT '字典类型标识',
  `description` varchar(255) DEFAULT NULL COMMENT '字典类型描述',
  `enable` char(1) DEFAULT NULL COMMENT '是否启用',
  `create_by` char(19) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` char(19) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1317360314219495424', '登录类型', 'sys_notice_type', '登录类型', '0', null, null, null, null, null);
INSERT INTO `sys_dict_type` VALUES ('1317400519127334912', '用户类型', 'user_status', '用户类型', '0', null, null, null, null, null);
INSERT INTO `sys_dict_type` VALUES ('1317400823096934400', '配置类型', 'config_type', '配置类型', '0', null, null, null, null, null);
INSERT INTO `sys_dict_type` VALUES ('1370410853110644736', '消息类型', 'sys_notice_type', '消息类型', '0', null, null, null, null, null);
INSERT INTO `sys_dict_type` VALUES ('455184568505470976', '用户性别', 'user_sex', '用户性别', '0', null, null, null, null, null);
INSERT INTO `sys_dict_type` VALUES ('455184935989415936', '全局状态', 'sys_status', '状态描述\n', '0', null, null, null, null, null);

-- ----------------------------
-- Table structure for `sys_file`
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file` (
  `id` varchar(255) NOT NULL COMMENT '标识',
  `file_name` varchar(255) DEFAULT NULL COMMENT '文件名称',
  `file_desc` varchar(255) DEFAULT NULL COMMENT '文件描述',
  `file_path` varchar(255) DEFAULT NULL COMMENT '文件路径',
  `file_type` varchar(255) DEFAULT NULL COMMENT '文件类型',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `target_date` datetime DEFAULT NULL COMMENT '所属时间',
  `file_size` varchar(255) DEFAULT NULL COMMENT '文件大小',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES ('1393082076084830208', '1393082076084830208.jpg', '1620971623000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393082076084830208.jpg', 'jpg', null, '2021-05-14 13:53:44', null, null, null, '2021-05-14 00:00:00', '93KB');
INSERT INTO `sys_file` VALUES ('1393116136098234368', '1393116136098234368.jpg', '1620979744000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393116136098234368.jpg', 'jpg', null, '2021-05-14 16:09:04', null, null, null, '2021-05-14 00:00:00', '154KB');
INSERT INTO `sys_file` VALUES ('1393116740614881280', '1393116740614881280.jpg', '1620979888000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393116740614881280.jpg', 'jpg', null, '2021-05-14 16:11:28', null, null, null, '2021-05-14 00:00:00', '152KB');
INSERT INTO `sys_file` VALUES ('1393123859716112384', '1393123859716112384.jpg', '1620981585000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393123859716112384.jpg', 'jpg', null, '2021-05-14 16:39:46', null, null, null, '2021-05-14 00:00:00', '93KB');
INSERT INTO `sys_file` VALUES ('1393124939799724032', '1393124939799724032.jpg', '1620981843000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393124939799724032.jpg', 'jpg', null, '2021-05-14 16:44:03', null, null, null, '2021-05-14 00:00:00', '152KB');
INSERT INTO `sys_file` VALUES ('1393125252795465728', '1393125252795465728.jpg', '1620981917000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393125252795465728.jpg', 'jpg', null, '2021-05-14 16:45:18', null, null, null, '2021-05-14 00:00:00', '93KB');
INSERT INTO `sys_file` VALUES ('1393125544572223488', '1393125544572223488.jpg', '1620981987000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393125544572223488.jpg', 'jpg', null, '2021-05-14 16:46:28', null, null, null, '2021-05-14 00:00:00', '93KB');
INSERT INTO `sys_file` VALUES ('1393125615187525632', '1393125615187525632.jpg', '1620982004000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393125615187525632.jpg', 'jpg', null, '2021-05-14 16:46:44', null, null, null, '2021-05-14 00:00:00', '93KB');
INSERT INTO `sys_file` VALUES ('1393129455936864256', '1393129455936864256.jpg', '1620982919000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393129455936864256.jpg', 'jpg', null, '2021-05-14 17:02:00', null, null, null, '2021-05-14 00:00:00', '152KB');
INSERT INTO `sys_file` VALUES ('1393129688590712832', '1393129688590712832.jpg', '1620982975000.jpg', 'D:\\home\\uploads\\2021-05-14\\1393129688590712832.jpg', 'jpg', null, '2021-05-14 17:02:56', null, null, null, '2021-05-14 00:00:00', '110KB');
INSERT INTO `sys_file` VALUES ('1394188889798737920', '1394188889798737920.jpg', '1621235508000.jpg', 'D:\\home\\uploads\\2021-05-17\\1394188889798737920.jpg', 'jpg', null, '2021-05-17 15:11:49', null, null, null, '2021-05-17 00:00:00', '92KB');
INSERT INTO `sys_file` VALUES ('1394199551404933120', '1394199551404933120.jpg', '1621238050000.jpg', 'D:\\home\\uploads\\2021-05-17\\1394199551404933120.jpg', 'jpg', null, '2021-05-17 15:54:11', null, null, null, '2021-05-17 00:00:00', '92KB');
INSERT INTO `sys_file` VALUES ('1394199648599539712', '1394199648599539712.jpg', '1621238073000.jpg', 'D:\\home\\uploads\\2021-05-17\\1394199648599539712.jpg', 'jpg', null, '2021-05-17 15:54:34', null, null, null, '2021-05-17 00:00:00', '153KB');
INSERT INTO `sys_file` VALUES ('1394913551281291264', '1394913551281291264.jpg', '1621408281000.jpg', 'D:\\home\\uploads\\2021-05-19\\1394913551281291264.jpg', 'jpg', null, '2021-05-19 15:11:22', null, null, null, '2021-05-19 00:00:00', '157KB');
INSERT INTO `sys_file` VALUES ('1394913679903817728', '1394913679903817728.jpg', '1621408312000.jpg', 'D:\\home\\uploads\\2021-05-19\\1394913679903817728.jpg', 'jpg', null, '2021-05-19 15:11:52', null, null, null, '2021-05-19 00:00:00', '154KB');
INSERT INTO `sys_file` VALUES ('1394925364236517376', '1394925364236517376.jpg', '1621411097000.jpg', 'D:\\home\\uploads\\2021-05-19\\1394925364236517376.jpg', 'jpg', null, '2021-05-19 15:58:18', null, null, null, '2021-05-19 00:00:00', '93KB');

-- ----------------------------
-- Table structure for `sys_logging`
-- ----------------------------
DROP TABLE IF EXISTS `sys_logging`;
CREATE TABLE `sys_logging` (
  `id` char(19) NOT NULL COMMENT '相应消息体',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `method` varchar(255) DEFAULT NULL COMMENT '请求方式',
  `business_type` varchar(255) DEFAULT NULL COMMENT '业务类型',
  `request_method` varchar(255) DEFAULT NULL COMMENT '请求方法',
  `operate_name` varchar(255) DEFAULT NULL COMMENT '操作人',
  `operate_url` varchar(255) DEFAULT NULL COMMENT '操作路径',
  `operate_address` varchar(255) DEFAULT NULL COMMENT '操作地址',
  `request_param` varchar(255) DEFAULT NULL COMMENT '请求参数',
  `response_body` varchar(255) DEFAULT NULL COMMENT '相应消息体',
  `success` varchar(255) DEFAULT NULL COMMENT '是否成功',
  `error_msg` text COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `request_body` varchar(255) DEFAULT NULL COMMENT '请求消息体',
  `browser` varchar(255) DEFAULT NULL COMMENT '使用浏览器',
  `system_os` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `logging_type` varchar(10) DEFAULT NULL COMMENT '日志类型，登录日志，操作日志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_logging
-- ----------------------------
INSERT INTO `sys_logging` VALUES ('1387093346635218944', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-04-28 01:16:39', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1387093352247197696', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-04-28 01:16:41', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1387093362711986176', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-04-28 01:16:43', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393081797478187008', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 13:52:37', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393081803283103744', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 13:52:39', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393081952466108416', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 13:53:14', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393082076529426432', '修改头像', '/system/user/updateAvatar', 'EDIT', 'PUT', 'admin', '/system/user/updateAvatar', '127.0.0.1', null, null, '1', null, '2021-05-14 13:53:44', '修改头像', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393082102513139712', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 13:53:50', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393082131248316416', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 13:53:57', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393082261410152448', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 13:54:28', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393082266543980544', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 13:54:29', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393082299695759360', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 13:54:37', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393082334432985088', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-14 13:54:45', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393082424748933120', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 13:55:07', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393087658984472576', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 14:15:55', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393087664705503232', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:15:56', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393087818854563840', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-14 14:16:33', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393090473106604032', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 14:27:06', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393090478693416960', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:27:07', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393090490550714368', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-14 14:27:10', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393094356130332672', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 14:42:32', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393094361729728512', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:42:33', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393094418805817344', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-14 14:42:47', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393095564496732160', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:47:20', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393095573904556032', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-14 14:47:22', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393095746026209280', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:48:03', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393095755392090112', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-14 14:48:05', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393095795741294592', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-14 14:48:15', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393096986852327424', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:52:59', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393097254650249216', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 14:54:03', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393097260237062144', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:54:04', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393097530283130880', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:55:08', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393097561262260224', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:55:16', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393098033553473536', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 14:57:08', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393098039157063680', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:57:10', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393098080965885952', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 14:57:20', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393102542690320384', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 15:15:03', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393102548109361152', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:15:05', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393105001265496064', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 15:24:50', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393105007103967232', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:24:51', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393105905892982784', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:28:25', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393106080111788032', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:29:07', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393106081151975424', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:29:07', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393106425110069248', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:30:29', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393108189737648128', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:37:30', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393109108827095040', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:41:09', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393109180243509248', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:41:26', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393110212474306560', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:45:32', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393110767179399168', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:47:44', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393111323046313984', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:49:57', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393112194626879488', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:53:25', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393112755673759744', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:55:38', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393112943805071360', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:56:23', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393113085635461120', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:56:57', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393113755637776384', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 15:59:37', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393114449379852288', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:02:22', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393114538223599616', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:02:43', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393114813151838208', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:03:49', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393114818277277696', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:03:50', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393115617447378944', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:07:01', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393116136576385024', '修改头像', '/system/user/updateAvatar', 'EDIT', 'PUT', 'admin', '/system/user/updateAvatar', '127.0.0.1', null, null, '1', null, '2021-05-14 16:09:04', '修改头像', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393116474121388032', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:10:25', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393116740677795840', '修改头像', '/system/user/updateAvatar', 'EDIT', 'PUT', 'admin', '/system/user/updateAvatar', '127.0.0.1', null, null, '1', null, '2021-05-14 16:11:29', '修改头像', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393118601052946432', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:18:52', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393118820733812736', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:19:44', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393118825867640832', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:19:46', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393119033561186304', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:20:35', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393120446043389952', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:26:12', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393120451638591488', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:26:13', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393120795378581504', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:27:35', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393123044326637568', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:36:31', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393123049951199232', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:36:33', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393123630342209536', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:38:51', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393123768091541504', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:39:24', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393123773728686080', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:39:25', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393124888033624064', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:43:51', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393124893658185728', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:43:52', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393126682847608832', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:50:59', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393126688006602752', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:51:00', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393127996579119104', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:56:12', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393128002224652288', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:56:13', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393128180105084928', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 16:56:56', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393128185222135808', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 16:56:57', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393129776134225920', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 17:03:16', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393133113181208576', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-14 17:16:32', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1393133118835130368', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 17:16:33', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1393136473963757568', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-14 17:29:53', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394108468033486848', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 09:52:15', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394108474375274496', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 09:52:16', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394112013034061824', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 10:06:20', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394112018855755776', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:06:21', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394112298313842688', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:07:28', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394112940193349632', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 10:10:01', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394112945968906240', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:10:02', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394113199594274816', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:11:03', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394113464795922432', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:12:06', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394114185964552192', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:14:58', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394114605554335744', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:16:38', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394119724605898752', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:36:58', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394119769682083840', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:37:09', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394119868541829120', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:37:33', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394120082996592640', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:38:24', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394120790118498304', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:41:13', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394120939423137792', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 10:41:48', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394120945211277312', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:41:50', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394121079412228096', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:42:22', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394121129697738752', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:42:33', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394122374567165952', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 10:47:30', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394122380304973824', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:47:32', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394122862306000896', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 10:49:27', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394122867670515712', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:49:28', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394123144519745536', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:50:34', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394123698268536832', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 10:52:46', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394123703914070016', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:52:47', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394124746722902016', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:56:56', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394125328674193408', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:59:15', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394125428800618496', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:59:38', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394125456218783744', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 10:59:45', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394135173771034624', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:38:22', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394135421444685824', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:39:21', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394135851868356608', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 11:41:04', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394135857664884736', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:41:05', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394136128017137664', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:42:09', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394136190340300800', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:42:24', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394136728893128704', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:44:33', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394137417031614464', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:47:17', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394137470773231616', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:47:30', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394137837376372736', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:48:57', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394137932318638080', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:49:20', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394138260170604544', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 11:50:38', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394144229374033920', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:14:21', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394144312165400576', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:14:41', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394144372345274368', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:14:55', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394144554558423040', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:15:38', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394145028980342784', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:17:32', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394145041470980096', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:17:35', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394145389187170304', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:18:57', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394145982282727424', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:21:19', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394146271446433792', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:22:28', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394146307223846912', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:22:36', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394147325881876480', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:26:39', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394149834998415360', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:36:37', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394150581152514048', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:39:35', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394150642238357504', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:39:50', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394150681845170176', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:39:59', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394150917753798656', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:40:56', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394150976331448320', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:41:09', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394151328950779904', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:42:34', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394151454712791040', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:43:04', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394151811937468416', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 12:44:29', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394163597986234368', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:31:19', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394163964266414080', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:32:46', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394164450142978048', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:34:42', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394165156988059648', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:37:30', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394165908217266176', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 13:40:30', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394165913611141120', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:40:31', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394165966174158848', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:40:43', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394166076673097728', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:41:10', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394166142154571776', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:41:25', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394166264716328960', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:41:55', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394166313638690816', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:42:06', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394166440881291264', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:42:37', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394167954458804224', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:48:37', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394167985853169664', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:48:45', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394168070087376896', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:49:05', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394168429920911360', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:50:31', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394168583604404224', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 13:51:07', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394168589317046272', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:51:09', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394169282576777216', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:53:54', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394169510692388864', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:54:48', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394169789198368768', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:55:55', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394169997495894016', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 13:56:44', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394170915230580736', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 14:00:23', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394172240819388416', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 14:05:39', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394172275715997696', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 14:05:48', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394172505630965760', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 14:06:42', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394172551239827456', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 14:06:53', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394178707584712704', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 14:31:21', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394178713423183872', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 14:31:23', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394178755185868800', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-17 14:31:32', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394188789248688128', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 15:11:25', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394188795032633344', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:11:26', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394190290901794816', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 15:17:23', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394190296262115328', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:17:24', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394190330726711296', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:17:32', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394193007929982976', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 15:28:11', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394193013797814272', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:28:12', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394197335105339392', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 15:45:22', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394197341384212480', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:45:24', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394197458430459904', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:45:52', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394197548071124992', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:46:13', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394198741610659840', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:50:58', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394198770073206784', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:51:04', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394198843293171712', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:51:22', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394198854215139328', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:51:24', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394199051766857728', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:52:12', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394200286112776192', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:57:06', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394200564690059264', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 15:58:12', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394200570570473472', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 15:58:14', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394202335315492864', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 16:05:14', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394202340810031104', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:05:16', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394203137849425920', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 16:08:26', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394203143633371136', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:08:27', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394203583255150592', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 16:10:12', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394203588955209728', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:10:13', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394204070113181696', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:12:08', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394204619629920256', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:14:19', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394204845119897600', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:15:13', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394205243679440896', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:16:48', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394205536508968960', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:17:58', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394205971005308928', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-17 16:19:41', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394205976462098432', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-17 16:19:43', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394834888162541568', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-19 09:58:47', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394834894294614016', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-19 09:58:48', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394834941593780224', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-19 09:59:00', '返回 Index 主页视图', null, '你用啥浏览器', 'Mac', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394836902707724288', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-19 10:06:47', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394836929249280000', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-19 10:06:53', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394888683286429696', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-19 13:32:33', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394888689854709760', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-19 13:32:34', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394888696724979712', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-19 13:32:36', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394892261522669568', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '127.0.0.1', null, null, '1', null, '2021-05-19 13:46:46', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394892267214340096', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '127.0.0.1', null, null, '1', null, '2021-05-19 13:46:47', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394892273400938496', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '127.0.0.1', null, null, '1', null, '2021-05-19 13:46:49', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394908725059256320', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '192.168.0.100', null, null, '1', null, '2021-05-19 14:52:11', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394908730931281920', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 14:52:12', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394912449051754496', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '192.168.0.100', null, null, '1', null, '2021-05-19 15:06:59', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394912455389347840', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 15:07:00', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394913280144703488', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 15:10:17', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394919022637613056', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '192.168.0.100', null, null, '1', null, '2021-05-19 15:33:06', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394919028694188032', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 15:33:07', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394919034138394624', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '192.168.0.100', null, null, '1', null, '2021-05-19 15:33:09', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394919092850262016', '修改用户', '/system/user/update', 'EDIT', 'PUT', 'admin', '/system/user/update', '192.168.0.100', null, null, '1', null, '2021-05-19 15:33:23', '修改用户', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394919098495795200', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '192.168.0.100', null, null, '1', null, '2021-05-19 15:33:24', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394919158004580352', '登录', '/login', 'OTHER', 'POST', '未登录用户', '/login', '192.168.0.100', null, null, '0', null, '2021-05-19 15:33:38', '账户密码不正确', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394919199024873472', '登录', '/login', 'OTHER', 'POST', '未登录用户', '/login', '192.168.0.100', null, null, '0', null, '2021-05-19 15:33:48', '账户密码不正确', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394919281447141376', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '192.168.0.100', null, null, '1', null, '2021-05-19 15:34:08', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394919286648078336', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 15:34:09', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394919291798683648', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '192.168.0.100', null, null, '1', null, '2021-05-19 15:34:10', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394919701712207872', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 15:35:48', '返回 Index 主页视图', null, '谷歌浏览器', 'Android', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394919718120325120', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '192.168.0.100', null, null, '1', null, '2021-05-19 15:35:52', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394924485945065472', '登录', '/login', 'OTHER', 'POST', 'admin', '/login', '192.168.0.100', null, null, '1', null, '2021-05-19 15:54:49', '登录成功', null, '谷歌浏览器', 'Windows', 'LOGIN');
INSERT INTO `sys_logging` VALUES ('1394924492056166400', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 15:54:50', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394924506434240512', '查询用户', '/system/user/data', 'QUERY', 'GET', 'admin', '/system/user/data', '192.168.0.100', null, null, '1', null, '2021-05-19 15:54:53', '查询用户', 'page=1&limit=10', '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394925364681113600', '修改头像', '/system/user/updateAvatar', 'EDIT', 'PUT', 'admin', '/system/user/updateAvatar', '192.168.0.100', null, null, '1', null, '2021-05-19 15:58:18', '修改头像', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394925694122721280', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 15:59:37', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');
INSERT INTO `sys_logging` VALUES ('1394925846690529280', '主页', '/index', 'ADD', 'GET', 'admin', '/index', '192.168.0.100', null, null, '1', null, '2021-05-19 16:00:13', '返回 Index 主页视图', null, '谷歌浏览器', 'Windows', 'OPERATE');

-- ----------------------------
-- Table structure for `sys_mail`
-- ----------------------------
DROP TABLE IF EXISTS `sys_mail`;
CREATE TABLE `sys_mail` (
  `mail_id` varchar(20) NOT NULL COMMENT '邮件id(主键)',
  `receiver` varchar(1024) NOT NULL DEFAULT '0' COMMENT '收件人邮箱',
  `subject` varchar(128) DEFAULT NULL COMMENT '邮件主体',
  `content` text NOT NULL COMMENT '邮件正文',
  `create_by` varchar(16) DEFAULT NULL COMMENT '发送人',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`mail_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_mail
-- ----------------------------
INSERT INTO `sys_mail` VALUES ('1349598576807772160', '1218600762@qq.com', 'macbook pro', '13.3寸\nm1处理器\n16G内存', 'admin', '2021-01-14 06:06:23');
INSERT INTO `sys_mail` VALUES ('1357215518368464896', 'BoscoKuo@aliyun.com', '湖人总冠军', '湖人总冠军', 'admin', '2021-02-04 06:33:36');
INSERT INTO `sys_mail` VALUES ('1357219037586653184', 'BoscoKuo@aliyun.com', 'LebronJames', 'Lakers', 'admin', '2021-02-04 06:47:35');

-- ----------------------------
-- Table structure for `sys_notice`
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `id` char(20) NOT NULL COMMENT '编号',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` text COMMENT '内容',
  `sender` char(20) DEFAULT NULL COMMENT '发送人',
  `accept` char(20) DEFAULT NULL COMMENT '接收者',
  `type` char(10) DEFAULT NULL COMMENT '类型',
  `create_by` char(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` char(20) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('1370769290961092608', '公告测试', '公告测试', '1309861917694623744', null, 'public', null, '2021-03-14 00:10:41', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370769348204953600', '私信测试', '私信测试', '1309861917694623744', '1310409555649232897', 'private', null, '2021-03-14 00:10:55', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370771980034244608', '公告测试', '公告测试', '1309861917694623744', null, 'public', null, '2021-03-14 00:21:22', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370772014771470336', '公告测试', '公告测试', '1309861917694623744', null, 'public', null, '2021-03-14 00:21:31', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370772050439831552', '公告测试', '公告测试', '1309861917694623744', null, 'public', null, '2021-03-14 00:21:39', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370772089446858752', '私信测试', '私信测试', '1309861917694623744', '1310409555649232897', 'private', null, '2021-03-14 00:21:48', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370772143918284800', '私信测试', '私信测试', '1309861917694623744', '1310409555649232897', 'private', null, '2021-03-14 00:22:01', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370772363838226432', '私信测试', '私信测试', '1309861917694623744', '1349021166525743105', 'private', null, '2021-03-14 00:22:54', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370772466212798464', '私信测试', '私信测试', '1309861917694623744', '1349021166525743105', 'private', null, '2021-03-14 00:23:18', null, null, null);
INSERT INTO `sys_notice` VALUES ('1370971086266564608', '私信测试', '私信测试', '1309861917694623744', '1309861917694623744', 'private', null, '2021-03-14 13:32:33', null, null, null);

-- ----------------------------
-- Table structure for `sys_power`
-- ----------------------------
DROP TABLE IF EXISTS `sys_power`;
CREATE TABLE `sys_power` (
  `power_id` char(19) NOT NULL COMMENT '权限编号',
  `power_name` varchar(255) DEFAULT NULL COMMENT '权限名称',
  `power_type` char(1) DEFAULT NULL COMMENT '权限类型',
  `power_code` char(30) DEFAULT NULL COMMENT '权限标识',
  `power_url` varchar(255) DEFAULT NULL COMMENT '权限路径',
  `open_type` char(10) DEFAULT NULL COMMENT '打开方式',
  `parent_id` char(19) DEFAULT NULL COMMENT '父类编号',
  `icon` varchar(128) DEFAULT NULL COMMENT '图标',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` char(19) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` char(19) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `enable` char(1) DEFAULT NULL COMMENT '是否开启',
  PRIMARY KEY (`power_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_power
-- ----------------------------
INSERT INTO `sys_power` VALUES ('1', '系统管理', '0', '', '', null, '0', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1284020948269268992', '用户列表', '2', 'sys:user:data', '', '', '2', 'layui-icon-username', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1284022967767924736', '用户保存', '2', 'sys:user:add', '', '', '2', 'layui-icon-username', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1302180351979814912', '布局构建', '1', 'generator:from:main', 'component/code/index.html', '_iframe', '442417411065516032', 'layui-icon-senior', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1304387665067507712', '数据字典', '1', 'sys:dictType:main', '/system/dictType/main', '_iframe', '1', 'layui-icon layui-icon layui-icon layui-icon layui-icon-flag', '4', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1304793451996381184', '文件管理', '1', 'sys:file:main', '/system/file/main', '_iframe', '1', 'layui-icon layui-icon layui-icon-read', '5', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1305870685385523200', '百度一下', '1', '', 'http://www.baidu.com', '0', '474356044148117504', 'layui-icon-search', '2', null, null, null, null, null, null);
INSERT INTO `sys_power` VALUES ('1305875436139446272', '百度一下', '1', 'http://www.baidu.com', 'http://www.baidu.com', '0', '451002662209589248', 'layui-icon-search', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1307299332784914432', '系统配置', '1', 'sys:config:main', '/system/config/main', '0', '1', 'layui-icon layui-icon layui-icon-note', '6', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1307562196556840960', '工作流程', '0', '', '', '0', '0', 'layui-icon layui-icon-chat', '5', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1307562519451140096', '模型管理', '1', '/process/model/main', '/process/model/main', '0', '1307562196556840960', 'layui-icon-circle', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1308571483794046976', '流程定义', '1', 'process:defined:main', '/process/defined/main', '0', '1307562196556840960', 'layui-icon-chart-screen', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310206853057085440', '用户修改', '2', 'sys:user:edit', '', '', '2', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310208636370288640', '用户删除', '2', 'sys:user:remove', '', '', '2', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310209696916832256', '角色新增', '2', 'sys:role:add', '', '', '3', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310209900478988288', '角色删除', '2', 'sys:role:remove', '', '', '3', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310210054728712192', '角色修改', '2', 'sys:role:edit', '', '', '3', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310211965188046848', '角色授权', '2', 'sys:role:power', '', '', '3', 'layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310226416867999744', '权限列表', '2', 'sys:power:data', '', '', '4', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310226976593674240', '权限新增', '2', 'sys:power:add', '', '', '4', 'layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310227130998587392', '权限修改', '2', 'sys:power:edit', '', '', '4', 'layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310227300935008256', '权限删除', '2', 'sys:power:remove', '', '', '4', 'layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310232350285627392', '操作日志', '2', 'sys:log:operateLog', '', '', '450300705362808832', 'layui-icon layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310232462562951168', '登录日志', '2', 'sys:log:loginLog', '', '', '450300705362808832', 'layui-icon layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310238229588344832', '配置列表', '2', 'sys:config:data', '', '', '1307299332784914432', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310238417082122240', '配置新增', '2', 'sys:config:add', '', '', '1307299332784914432', 'layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310238574355939328', '配置修改', '2', 'sys:config:edit', '', '', '1307299332784914432', 'layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310238700705153024', '配置删除', '2', 'sys:config:remove', '', '', '1307299332784914432', 'layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310243862937075712', '文件列表', '2', 'sys:file:data', '', '', '1304793451996381184', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310244103824343040', '文件新增', '2', 'sys:file:add', '', '', '1304793451996381184', 'layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310244248884346880', '文件删除', '2', 'sys:file:remove', '', '', '1304793451996381184', 'layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310390699333517312', '任务列表', '2', 'sch:job:data', '', '', '442650770626711552', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310390994826428416', '任务新增', '2', 'sch:job:add', '', '', '442650770626711552', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310391095670079488', '任务修改', '2', 'sch:job:edit', '', '', '442650770626711552', 'layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310391707069579264', '任务删除', '2', 'sch:job:remove', '', '', '442650770626711552', 'layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310395250908332032', '日志列表', '2', 'sch:log:data', '', '', '442651158935375872', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310397832091402240', '任务恢复', '2', 'sch:job:resume', '', '', '442650770626711552', 'layui-icon-vercode', null, null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310398020692475904', '任务停止', '2', 'sch:job:pause', '', '', '442650770626711552', 'layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310398158974484480', '任务运行', '2', 'sch:job:run', '', '', '442650770626711552', 'layui-icon-vercode', '4', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310402491631796224', '数据类型列表', '2', 'sys:dictType:data', '', '', '1304387665067507712', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310402688881524736', '数据类型新增', '2', 'sys:dictType:add', '', '', '1304387665067507712', 'layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310402817776680960', '数据类型修改', '2', 'sys:dictType:edit', '', '', '1304387665067507712', 'layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310403004406431744', '数据类型删除', '2', 'sys:dictType:remove', '', '', '1304387665067507712', 'layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310404584291696640', '数据字典视图', '2', 'sys:dictData:main', '', '', '1304387665067507712', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310404705934901248', '数据字典列表', '2', 'sys:dictData:data', '', '', '1304387665067507712', 'layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310404831407505408', '数据字典新增', '2', 'sys:dictData:add', '', '', '1304387665067507712', 'layui-icon-vercode', '5', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310404999599095808', '数据字典删除', '2', 'sys:dictData:remove', '', '', '1304387665067507712', 'layui-icon-vercode', '6', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1310405161587310592', '数据字典修改', '2', 'sys:dictData:edit', '', '', '1304387665067507712', 'layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1313142510486290432', '公告列表', '1', 'sys:notice:data', '/system/notice/data', '0', '1313142171393589248', 'layui-icon-notice', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1313482983558086656', '公告新增', '2', 'sys:notice:add', '', '', '1313142171393589248', 'layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1313483090852577280', '公告修改', '2', 'sys:notice:edit', '', '', '1313142171393589248', 'layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1313483189850734592', '公告删除', '2', 'sys:notice:remove', '', '', '1313142171393589248', 'layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1315584471046553600', '部门管理', '1', 'sys:dept:main', '/system/dept/main', '_iframe', '1', 'layui-icon layui-icon layui-icon layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1316558444790022144', '部门新增', '2', 'sys:dept:add', '', '', '1315584471046553600', 'layui-icon layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1316558556102656000', '部门修改', '2', 'sys:dept:edit', '', '', '1315584471046553600', 'layui-icon layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1316558685442408448', '部门删除', '2', 'sys:dept:remove', '', '', '1315584471046553600', 'layui-icon layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1317555660455411712', '部门列表', '2', 'sys:dept:data', '', '', '1315584471046553600', 'layui-icon layui-icon layui-icon layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1318229908526202880', '模型新增', '2', 'pro:model:add', '', '', '1307562519451140096', 'layui-icon layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1318230013262168064', '模型修改', '2', 'pro:model:edit', '', '', '1307562519451140096', 'layui-icon layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1318230265385975808', '模型删除', '2', 'pro:model:remove', '', '', '1307562519451140096', 'layui-icon layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1320969572051845120', '111111', '2', '', '', '', '1284020948269268992', 'layui-icon-login-qq', null, null, null, null, null, null, null);
INSERT INTO `sys_power` VALUES ('1322085079861690368', '用户管理', '1', 'sys:user:main', '/system/user/main', '_iframe', '1', 'layui-icon layui-icon layui-icon layui-icon layui-icon-rate', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1322085270392143872', '用户列表', '2', 'sys:user:data', '', '', '1322085079861690368', 'layui-icon layui-icon layui-icon layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1322085393021009920', '用户新增', '2', 'sys:user:add', '', '', '1322085079861690368', 'layui-icon layui-icon-vercode', null, null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1322085497798918144', '用户修改', '2', 'sys:user:edit', '', '', '1322085079861690368', 'layui-icon layui-icon layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1322085659766161408', '用户删除', '2', 'sys:user:remove', '', '', '1322085079861690368', 'layui-icon layui-icon-rate', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1329349076189184000', '', '1', '', '', '', '451002662209589248', 'layui-icon', null, null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1330865171429588992', '在线用户', '1', 'sys:online:main', '/system/online/main', '_iframe', '694203021537574912', 'layui-icon layui-icon layui-icon-username', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1348562759603716096', '在线列表', '1', 'sys:online:data', '/system/online/data', '_iframe', '1330865171429588992', 'layui-icon layui-icon-username', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1349016358033031168', '环境监控', '1', 'sys:monitor:main', '/system/monitor/main', '_iframe', '694203021537574912', 'layui-icon layui-icon-vercode', '9', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1349279791521464320', '电子邮件', '1', 'sys:mail:main', '/system/mail/main', '_iframe', '1', 'layui-icon layui-icon layui-icon layui-icon-list', '7', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1349636574442160128', '邮件发送', '2', 'sys:mail:save', '', '', '1349279791521464320', 'layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-vercode', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1349636919478190080', '邮件删除', '2', 'sys:mail:remove', '', '', '1349279791521464320', 'layui-icon layui-icon layui-icon layui-icon layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1349637786285637632', '邮件列表', '2', 'sys:mail:data', '', '', '1349279791521464320', 'layui-icon layui-icon layui-icon-vercode', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1349638479767666688', '邮件新增', '2', 'sys:mail:add', '', '', '1349279791521464320', 'layui-icon layui-icon layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1355962888132493312', '系统设置', '1', 'sys:setup:main', '/system/setup/main', '_iframe', '1', 'layui-icon layui-icon layui-icon-set', '11', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1370412169610395648', '站内消息', '1', 'system:notice:main', '/system/notice/main', '_iframe', '1', 'layui-icon layui-icon layui-icon layui-icon-set-fill', '8', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1370412169610395649', '消息列表', '2', 'system:notice:data', '', null, '1370412169610395648', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1370412169610395650', '消息新增', '2', 'system:notice:add', '', null, '1370412169610395648', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1370412169610395651', '消息修改', '2', 'system:notice:edit', '', null, '1370412169610395648', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1370412169610395652', '消息删除', '2', 'system:notice:remove', '', null, '1370412169610395648', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1370974716822552576', '修改设置', '2', 'sys:setup:add', '', '', '1355962888132493312', 'layui-icon layui-icon-vercode', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201162280960', '考勤记录', '1', 'system:lastPeopleTime:main', '/system/lastPeopleTime/main', '_iframe', '1393095524944445440', 'layui-icon layui-icon-more', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201162280961', '列表', '2', 'system:lastPeopleTime:data', '', null, '1393091201162280960', 'layui-icon layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201162280962', '新增', '2', 'system:lastPeopleTime:add', '', null, '1393091201162280960', 'layui-icon layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201162280963', '修改', '2', 'system:lastPeopleTime:edit', '', null, '1393091201162280960', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201162280964', '删除', '2', 'system:lastPeopleTime:remove', '', null, '1393091201162280960', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201489436672', '考勤名单', '1', 'system:nameUrl:main', '/system/nameUrl/main', '_iframe', '1393095524944445440', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201489436673', '列表', '2', 'system:nameUrl:data', '', null, '1393091201489436672', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201489436674', '新增', '2', 'system:nameUrl:add', '', null, '1393091201489436672', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201489436675', '修改', '2', 'system:nameUrl:edit', '', null, '1393091201489436672', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201489436676', '删除', '2', 'system:nameUrl:remove', '', null, '1393091201489436672', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201569128448', '考勤时段', '1', 'system:time:main', '/system/sctime/main', '_iframe', '1393095524944445440', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201569128449', '列表', '2', 'system:time:data', '', null, '1393091201569128448', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201569128450', '新增', '2', 'system:time:add', '', null, '1393091201569128448', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201569128451', '修改', '2', 'system:time:edit', '', null, '1393091201569128448', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393091201569128452', '删除', '2', 'system:time:remove', '', null, '1393091201569128448', 'layui-icon layui-icon-set-fill', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('1393095524944445440', '考勤管理', '0', '', '', '', '0', 'layui-icon layui-icon-username', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('2', '用户管理', '2', '', '', '_iframe', '1320969572051845120', 'layui-icon layui-icon layui-icon-username', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('3', '角色管理', '1', 'sys:role:main', '/system/role/main', '_iframe', '1', 'layui-icon layui-icon-user', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('4', '权限管理', '1', 'sys:power:main', '/system/power/main', '_iframe', '1', 'layui-icon layui-icon-vercode', '2', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('442359447487123456', '角色列表', '2', 'sys:role:data', '', '', '3', 'layui-icon layui-icon-rate', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('442417411065516032', '开发工具', '0', '', '', '', '0', 'layui-icon layui-icon layui-icon-senior', '4', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('442418188639145984', '代码生成', '1', 'exp:template:main', '/generate/main', '_iframe', '442417411065516032', 'layui-icon layui-icon layui-icon layui-icon layui-icon layui-icon-template-1', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('442650770626711552', '定时任务', '1', 'sch:job:main', '/schedule/job/main', '_iframe', '694203021537574912', 'layui-icon layui-icon layui-icon layui-icon  layui-icon-chat', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('442651158935375872', '任务日志', '1', 'sch:log:main', '/schedule/log/main', '_iframe', '694203021537574912', 'layui-icon layui-icon layui-icon  layui-icon-file', '1', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('450300705362808832', '行为日志', '1', 'sys:log:main', '/system/log/main', '_iframe', '694203021537574912', 'layui-icon layui-icon layui-icon layui-icon  layui-icon-chart', '7', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('451002662209589248', '工作空间', '1', '', '', '', '451002662209589248', 'layui-icon layui-icon layui-icon-home', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('451003242072117248', '项目总览', '1', 'process:model:main', '/console', '_iframe', '451002662209589248', 'layui-icon  layui-icon-component', '0', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('474356363552755712', '项目介绍', '1', 'home', '/console', '_iframe', '474356044148117504', 'layui-icon layui-icon-home', '1', null, null, null, null, null, '0');
INSERT INTO `sys_power` VALUES ('694203021537574912', '系统监控', '0', '', '', '', '0', 'layui-icon  layui-icon-console', '3', null, null, null, null, null, '1');
INSERT INTO `sys_power` VALUES ('694203311615639552', '接口文档', '1', 'sys:doc:main', '/system/doc/main', '_iframe', '442417411065516032', 'layui-icon layui-icon layui-icon layui-icon  layui-icon-chart', '9', null, null, null, null, null, '1');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` char(19) NOT NULL COMMENT '角色编号',
  `role_name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `role_code` varchar(255) DEFAULT NULL COMMENT '角色标识',
  `enable` char(1) DEFAULT NULL COMMENT '是否启用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` char(19) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` char(19) DEFAULT NULL COMMENT '修改人',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `details` varchar(255) DEFAULT NULL COMMENT '详情',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1309851245195821056', '超级管理员', 'admin', '0', null, null, null, null, null, '超级管理员', '1');
INSERT INTO `sys_role` VALUES ('1313761100243664896', '普通管理员', 'manager', '0', null, null, null, null, null, '普通管理员', '2');
INSERT INTO `sys_role` VALUES ('1356112133691015168', '应急管理员', 'users', '0', null, null, null, null, null, '应急管理员', '2');

-- ----------------------------
-- Table structure for `sys_role_power`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_power`;
CREATE TABLE `sys_role_power` (
  `id` char(19) NOT NULL,
  `role_id` char(19) DEFAULT NULL,
  `power_id` char(19) DEFAULT NULL,
  `create_by` char(19) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_by` char(19) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_role_power
-- ----------------------------
INSERT INTO `sys_role_power` VALUES ('1284022485632679936', '3', '474356044148117504', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679937', '3', '474356363552755712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679938', '3', '1', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679939', '3', '3', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679940', '3', '442359447487123456', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679941', '3', '4', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679942', '3', '442722702474743808', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679943', '3', '2', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679944', '3', '1284020948269268992', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679945', '3', '442417411065516032', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679946', '3', '442418188639145984', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679947', '3', '694203021537574912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679948', '3', '450300705362808832', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679949', '3', '442520236248403968', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679950', '3', '694203311615639552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679951', '3', '442650387514789888', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679952', '3', '442650770626711552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679953', '3', '442651158935375872', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679954', '3', '451002662209589248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1284022485632679955', '3', '451003242072117248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650364506112', '2', '474356044148117504', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700416', '2', '474356363552755712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700417', '2', '2', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700418', '2', '1284020948269268992', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700419', '2', '450300705362808832', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700420', '2', '442417411065516032', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700421', '2', '442418188639145984', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700422', '2', '694203021537574912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700423', '2', '442520236248403968', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700424', '2', '694203311615639552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700425', '2', '442650387514789888', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700426', '2', '442650770626711552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700427', '2', '442651158935375872', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700428', '2', '451002662209589248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1305379650368700429', '2', '451003242072117248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380352', '1', '451002662209589248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380353', '1', '451003242072117248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380354', '1', '1305875436139446272', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380355', '1', '1', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380356', '1', '2', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380357', '1', '1284020948269268992', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380358', '1', '1284022967767924736', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380359', '1', '3', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380360', '1', '442359447487123456', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380361', '1', '4', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380362', '1', '1304387665067507712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380363', '1', '450300705362808832', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380364', '1', '1304793451996381184', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380365', '1', '1307299332784914432', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380366', '1', '442650387514789888', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380367', '1', '442650770626711552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380368', '1', '442651158935375872', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380369', '1', '694203021537574912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380370', '1', '442520236248403968', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380371', '1', '694203311615639552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380372', '1', '442417411065516032', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380373', '1', '442418188639145984', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380374', '1', '1302180351979814912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380375', '1', '1307562196556840960', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380376', '1', '1307562519451140096', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1308571532737380377', '1', '1308571483794046976', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897792', '1310215420371795968', '451002662209589248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897793', '1310215420371795968', '1', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897794', '1310215420371795968', '2', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897795', '1310215420371795968', '3', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897796', '1310215420371795968', '4', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897797', '1310215420371795968', '1304387665067507712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897798', '1310215420371795968', '450300705362808832', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897799', '1310215420371795968', '1304793451996381184', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897800', '1310215420371795968', '1307299332784914432', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897801', '1310215420371795968', '1313142171393589248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897802', '1310215420371795968', '1313142510486290432', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897803', '1310215420371795968', '442650387514789888', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897804', '1310215420371795968', '694203021537574912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897805', '1310215420371795968', '442417411065516032', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1313147486356897806', '1310215420371795968', '1307562196556840960', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1320969221462556672', '1320969145759563776', '451002662209589248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1320969221462556673', '1320969145759563776', '451003242072117248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1320969221462556674', '1320969145759563776', '1305875436139446272', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778112', '1309851245195821056', '1', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778113', '1309851245195821056', '1322085079861690368', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778114', '1309851245195821056', '1322085393021009920', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778115', '1309851245195821056', '1322085270392143872', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778116', '1309851245195821056', '1322085497798918144', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778117', '1309851245195821056', '1322085659766161408', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778118', '1309851245195821056', '3', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778119', '1309851245195821056', '1310209696916832256', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778120', '1309851245195821056', '1310209900478988288', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778121', '1309851245195821056', '1310210054728712192', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778122', '1309851245195821056', '442359447487123456', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778123', '1309851245195821056', '1310211965188046848', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778124', '1309851245195821056', '4', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778125', '1309851245195821056', '1310226416867999744', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778126', '1309851245195821056', '1310226976593674240', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778127', '1309851245195821056', '1310227130998587392', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778128', '1309851245195821056', '1310227300935008256', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778129', '1309851245195821056', '1315584471046553600', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778130', '1309851245195821056', '1316558444790022144', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778131', '1309851245195821056', '1316558556102656000', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778132', '1309851245195821056', '1317555660455411712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778133', '1309851245195821056', '1316558685442408448', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778134', '1309851245195821056', '1304387665067507712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778135', '1309851245195821056', '1310405161587310592', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778136', '1309851245195821056', '1310402491631796224', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778137', '1309851245195821056', '1310404584291696640', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778138', '1309851245195821056', '1310404705934901248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778139', '1309851245195821056', '1310402688881524736', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778140', '1309851245195821056', '1310402817776680960', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778141', '1309851245195821056', '1310403004406431744', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778142', '1309851245195821056', '1310404831407505408', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778143', '1309851245195821056', '1310404999599095808', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778144', '1309851245195821056', '1304793451996381184', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778145', '1309851245195821056', '1310243862937075712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778146', '1309851245195821056', '1310244103824343040', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778147', '1309851245195821056', '1310244248884346880', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778148', '1309851245195821056', '1307299332784914432', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778149', '1309851245195821056', '1310238229588344832', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778150', '1309851245195821056', '1310238417082122240', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778151', '1309851245195821056', '1310238574355939328', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778152', '1309851245195821056', '1310238700705153024', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778153', '1309851245195821056', '1349279791521464320', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778154', '1309851245195821056', '1349637786285637632', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778155', '1309851245195821056', '1349638479767666688', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778156', '1309851245195821056', '1349636919478190080', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778157', '1309851245195821056', '1349636574442160128', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778158', '1309851245195821056', '1355962888132493312', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778159', '1309851245195821056', '694203021537574912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778160', '1309851245195821056', '1330865171429588992', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778161', '1309851245195821056', '1348562759603716096', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778162', '1309851245195821056', '442650770626711552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778163', '1309851245195821056', '1310397832091402240', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778164', '1309851245195821056', '1310390699333517312', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778165', '1309851245195821056', '1310390994826428416', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778166', '1309851245195821056', '1310391095670079488', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778167', '1309851245195821056', '1310391707069579264', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778168', '1309851245195821056', '1310398020692475904', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778169', '1309851245195821056', '1310398158974484480', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778170', '1309851245195821056', '442651158935375872', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778171', '1309851245195821056', '1310395250908332032', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778172', '1309851245195821056', '450300705362808832', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778173', '1309851245195821056', '1310232350285627392', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778174', '1309851245195821056', '1310232462562951168', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778175', '1309851245195821056', '1349016358033031168', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778176', '1309851245195821056', '442417411065516032', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778177', '1309851245195821056', '442418188639145984', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778178', '1309851245195821056', '1302180351979814912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778179', '1309851245195821056', '694203311615639552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778180', '1309851245195821056', '1307562196556840960', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778181', '1309851245195821056', '1307562519451140096', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778182', '1309851245195821056', '1318229908526202880', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778183', '1309851245195821056', '1318230013262168064', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778184', '1309851245195821056', '1318230265385975808', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1355962953458778185', '1309851245195821056', '1308571483794046976', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712128', '1313761100243664896', '1', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712129', '1313761100243664896', '1322085079861690368', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712130', '1313761100243664896', '1322085393021009920', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712131', '1313761100243664896', '1322085270392143872', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712132', '1313761100243664896', '1322085497798918144', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712133', '1313761100243664896', '1322085659766161408', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712134', '1313761100243664896', '3', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712135', '1313761100243664896', '1310209696916832256', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712136', '1313761100243664896', '1310209900478988288', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712137', '1313761100243664896', '1310210054728712192', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712138', '1313761100243664896', '442359447487123456', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712139', '1313761100243664896', '1310211965188046848', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712140', '1313761100243664896', '4', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712141', '1313761100243664896', '1310226416867999744', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712142', '1313761100243664896', '1310226976593674240', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712143', '1313761100243664896', '1310227130998587392', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712144', '1313761100243664896', '1310227300935008256', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712145', '1313761100243664896', '1315584471046553600', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712146', '1313761100243664896', '1316558444790022144', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712147', '1313761100243664896', '1316558556102656000', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712148', '1313761100243664896', '1317555660455411712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712149', '1313761100243664896', '1316558685442408448', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712150', '1313761100243664896', '1304387665067507712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712151', '1313761100243664896', '1310402491631796224', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712152', '1313761100243664896', '1310404584291696640', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712153', '1313761100243664896', '1310405161587310592', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712154', '1313761100243664896', '1310402688881524736', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712155', '1313761100243664896', '1310404705934901248', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712156', '1313761100243664896', '1310402817776680960', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712157', '1313761100243664896', '1310403004406431744', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712158', '1313761100243664896', '1310404831407505408', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712159', '1313761100243664896', '1310404999599095808', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712160', '1313761100243664896', '1304793451996381184', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712161', '1313761100243664896', '1310243862937075712', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712162', '1313761100243664896', '1310244103824343040', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712163', '1313761100243664896', '1310244248884346880', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712164', '1313761100243664896', '1307299332784914432', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712165', '1313761100243664896', '1310238229588344832', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712166', '1313761100243664896', '1310238417082122240', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712167', '1313761100243664896', '1310238574355939328', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712168', '1313761100243664896', '1310238700705153024', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712169', '1313761100243664896', '1349279791521464320', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712170', '1313761100243664896', '1349637786285637632', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712171', '1313761100243664896', '1349638479767666688', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712172', '1313761100243664896', '1349636919478190080', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712173', '1313761100243664896', '1349636574442160128', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712174', '1313761100243664896', '1355962888132493312', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712175', '1313761100243664896', '1370974716822552576', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712176', '1313761100243664896', '694203021537574912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712177', '1313761100243664896', '1330865171429588992', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712178', '1313761100243664896', '442650770626711552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712179', '1313761100243664896', '442651158935375872', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712180', '1313761100243664896', '450300705362808832', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712181', '1313761100243664896', '1349016358033031168', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712182', '1313761100243664896', '442417411065516032', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712183', '1313761100243664896', '442418188639145984', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712184', '1313761100243664896', '1302180351979814912', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712185', '1313761100243664896', '694203311615639552', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712186', '1313761100243664896', '1307562196556840960', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712187', '1313761100243664896', '1307562519451140096', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712188', '1313761100243664896', '1318229908526202880', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712189', '1313761100243664896', '1318230013262168064', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712190', '1313761100243664896', '1318230265385975808', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('1370974927745712191', '1313761100243664896', '1308571483794046976', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('442062615250866176', '693913251020275712', '1', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('442062615250866177', '693913251020275712', '2', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('442062615250866178', '693913251020275712', '3', null, null, null, null, null);
INSERT INTO `sys_role_power` VALUES ('442062615250866179', '693913251020275712', '4', null, null, null, null, null);

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` char(19) NOT NULL COMMENT '编号',
  `username` char(20) DEFAULT NULL COMMENT '账户',
  `password` char(60) DEFAULT NULL COMMENT '密码',
  `salt` char(10) DEFAULT NULL COMMENT '姓名',
  `status` char(1) DEFAULT NULL COMMENT '状态',
  `real_name` char(8) DEFAULT NULL COMMENT '姓名',
  `email` char(20) DEFAULT NULL COMMENT '邮箱',
  `avatar` varchar(30) DEFAULT NULL COMMENT '头像',
  `sex` char(1) DEFAULT NULL COMMENT '性别',
  `phone` char(11) DEFAULT NULL COMMENT '电话',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` char(1) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` char(1) DEFAULT NULL COMMENT '修改人',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `enable` char(1) DEFAULT NULL COMMENT '是否启用',
  `login` char(1) DEFAULT NULL COMMENT '是否登录',
  `dept_id` char(19) DEFAULT NULL COMMENT '部门编号',
  `last_time` datetime DEFAULT NULL COMMENT '最后一次登录时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1306230031168569344', 'feng', '$2a$10$jjf2h8Cx2lkFMKy3NY9pguADYAMewyPr2IJw8YAI5zSH2/0R/9Kra', null, '1', '风筝', 'feng@gmail.com', null, '0', '15553726531', '2000-02-09 00:00:00', null, null, null, '被岁月镂空，亦受其雕琢', '1', null, '1', null);
INSERT INTO `sys_user` VALUES ('1309861917694623744', 'admin', '$2a$10$6T.NGloFO.mD/QOAUelMTOcjAH8N49h34TsXduDVlnNMrASIGBNz6', null, '1', '管理', 'Jmys1992@qq.com', '1394925364236517376', '0', '15553726531', '2020-09-26 22:26:32', null, null, null, '被岁月镂空，亦受其雕琢', '1', null, '1', '2021-05-19 15:54:49');
INSERT INTO `sys_user` VALUES ('1310409555649232897', 'ruhua', '$2a$10$pkvLdCLdFp2sXZpmK34wveekbWvHinW2ldBnic4SqjiKO8jK4Etka', null, '1', '如花', 'ruhua@gmail.com', null, '0', '15553726531', '2020-09-28 10:42:39', null, null, null, null, '1', null, '1', null);
INSERT INTO `sys_user` VALUES ('1349016976730619905', 'mwj', '$2a$10$mD0pnwOGjmOKihboidaTveUdrqcDYoluzfCOA0Ho87iwr9PKrDA6i', null, '1', '风筝', '', null, '1', '666666666', '2021-01-12 23:34:45', null, null, null, null, '1', null, '6', '2021-01-12 23:35:12');
INSERT INTO `sys_user` VALUES ('1349021166525743105', 'xiana', '$2a$10$6VuyGmiEbIix/gPDU8oe3O7DZSxGVByjXCHQGtyEMoRAt74M/daee', null, '1', '夏娜', 'xiana@gmail.com', null, '0', '15553726531', '2021-01-12 23:51:24', null, null, null, null, '1', null, '1', null);
INSERT INTO `sys_user` VALUES ('1355966975355912193', 'sanman', '$2a$10$AD3QnQMRhYY7RUDHd1EEL.KHaDW8/S66SsESwh.9ta8bLiUXrZcJe', null, '1', '散漫', 'sanman@gmail.com', null, '0', '15553726531', '2021-02-01 03:51:34', null, null, null, null, '1', null, '1', null);
INSERT INTO `sys_user` VALUES ('1355967204012589057', 'langhua', '$2a$10$MNbf6dSvvncpoPsNFyMW6ObPwfj3jCKsZa7LvVAiXco1DWtgA46he', null, '1', '浪花', 'langhua@gmail.com', null, '0', '15553726531', '2021-02-01 03:52:29', null, null, null, null, '1', null, '1', null);
INSERT INTO `sys_user` VALUES ('1355967579994193921', 'zidian', '$2a$10$c9OatFOMGnj37A6UJTwfGOKqCwCx50K8eZsjV5YoBRlpYHcz8WfyW', null, '1', '字典', 'zidian', null, '0', '15553726531', '2021-02-01 03:53:58', null, null, null, null, '1', null, '1', null);
INSERT INTO `sys_user` VALUES ('1370973608502886401', 'duanlang', '$2a$10$XNcKlX3AnXR/Gh2g8aLX5OFtLD69Yjl1O8PDLmITH4WCQT.shsrWe', null, '1', '断浪', 'duanlang@gmail.com', null, '0', '15553726531', '2021-03-14 13:42:34', null, null, null, null, '1', null, '1', '2021-03-14 13:47:28');
INSERT INTO `sys_user` VALUES ('1394923406683537408', 'aaa', '$2a$10$OPZp1tPl0T07I1TRcWabe.gOhASDgKttmz4MwNN7KWxtG20eLUI6S', null, '1', null, null, null, null, null, '2021-05-19 15:50:31', null, null, null, null, '1', null, '1', null);
INSERT INTO `sys_user` VALUES ('1394925050699710464', 'qqq', '$2a$10$3C/pG4VM.E.XjGstcL9HBOgT8S2tpvDzawdcF5l95uzePvCP9/4PK', null, '1', null, null, null, null, null, '2021-05-19 15:57:03', null, null, null, null, '1', null, '1', null);

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` char(19) NOT NULL COMMENT '标识',
  `user_id` char(19) DEFAULT NULL COMMENT '用户编号',
  `role_id` char(19) DEFAULT NULL COMMENT '角色编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1302196622322565120', '1302196622007992320', '1');
INSERT INTO `sys_user_role` VALUES ('1304443027040763904', '1304443026482921472', '1');
INSERT INTO `sys_user_role` VALUES ('1304443027044958208', '1304443026482921472', '2');
INSERT INTO `sys_user_role` VALUES ('1304443027044958209', '1304443026482921472', '3');
INSERT INTO `sys_user_role` VALUES ('1304443307404820480', '1304443306888921088', '1');
INSERT INTO `sys_user_role` VALUES ('1304443307404820481', '1304443306888921088', '2');
INSERT INTO `sys_user_role` VALUES ('1305359805342285824', '1305359804906078208', '');
INSERT INTO `sys_user_role` VALUES ('1305359807724650496', '1305359807296831488', '');
INSERT INTO `sys_user_role` VALUES ('1305390235135246336', '1305390234694844416', '');
INSERT INTO `sys_user_role` VALUES ('1306229860422647808', '1306229859755753472', '1');
INSERT INTO `sys_user_role` VALUES ('1306229892144168960', '1306229891624075264', '1');
INSERT INTO `sys_user_role` VALUES ('1306243520893288448', '1306243520482246656', '');
INSERT INTO `sys_user_role` VALUES ('1308074663896678400', '1308074663313670144', '1');
INSERT INTO `sys_user_role` VALUES ('1308074663896678401', '1308074663313670144', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1308074663896678402', '1308074663313670144', '2');
INSERT INTO `sys_user_role` VALUES ('1308075167091523584', '1308075166433017856', '1');
INSERT INTO `sys_user_role` VALUES ('1308075167091523585', '1308075166433017856', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1308075241188098048', '1308074939114323968', '1');
INSERT INTO `sys_user_role` VALUES ('1308075241188098049', '1308074939114323968', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1308075407685189632', '1308075407114764288', '1');
INSERT INTO `sys_user_role` VALUES ('1308075407685189633', '1308075407114764288', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1308075638158000128', '1308075637621129216', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1308328954523811840', '1308328954045661184', '1');
INSERT INTO `sys_user_role` VALUES ('1308328954523811841', '1308328954045661184', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1308328954523811842', '1308328954045661184', '2');
INSERT INTO `sys_user_role` VALUES ('1308571264494862336', '1308076162903179264', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1309445423668133888', '1309444883659882496', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1309445423668133889', '1309444883659882496', '1309121036125470720');
INSERT INTO `sys_user_role` VALUES ('1309445423668133890', '1309444883659882496', '2');
INSERT INTO `sys_user_role` VALUES ('1309752526945386496', '1', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1309752526945386497', '1', '1309121036125470720');
INSERT INTO `sys_user_role` VALUES ('1309752526945386498', '1', '2');
INSERT INTO `sys_user_role` VALUES ('1309860016655695872', '1309860016043327488', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1309860554432577536', '1309860553891512320', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1309861324494209024', '1309861323898617856', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1309861325593116672', '1309861324909445120', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1310080380040118272', '1310080379331280896', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1310080380589572096', '1310080379935260672', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1310080718918909952', '1310080718256209920', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1310080719917154304', '1310080719208316928', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1310082314557980672', '1310082313954000896', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1310082315195514880', '1310082314545397760', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1310083089153654784', '1310083088511926272', '1309121036125470720');
INSERT INTO `sys_user_role` VALUES ('1310083089828937728', '1310083089216569344', '1309121036125470720');
INSERT INTO `sys_user_role` VALUES ('1310083324709961728', '1310083324110176256', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1310208453033066496', '1310208452424892416', '1309121036125470720');
INSERT INTO `sys_user_role` VALUES ('1310209026096627712', '1310209025576534016', '1306230258952830976');
INSERT INTO `sys_user_role` VALUES ('1310209026096627713', '1310209025576534016', '1309121036125470720');
INSERT INTO `sys_user_role` VALUES ('1310381721815875584', '1306229381332467712', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1310424875067768832', '1310421836906889217', '1310421428759166976');
INSERT INTO `sys_user_role` VALUES ('1314015448013996032', '1304491590080790528', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1314410103465574400', '1314410059245027329', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1314416691479838720', '1314416690875858945', '');
INSERT INTO `sys_user_role` VALUES ('1316275764227735552', '1316275763711836161', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1316275764227735553', '1316275763711836161', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1316275899439513600', '1315827004456566785', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1316275899439513601', '1315827004456566785', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1316275930657718272', '1315829324519047169', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1316276059032780800', '1310409555649232897', '');
INSERT INTO `sys_user_role` VALUES ('1316410619078901760', '1306229606205882368', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1316410619078901761', '1306229606205882368', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1316410619078901762', '1306229606205882368', '1316407534105395200');
INSERT INTO `sys_user_role` VALUES ('1316410619078901763', '1306229606205882368', '1316408008376320000');
INSERT INTO `sys_user_role` VALUES ('1318205966671413248', '1318205965996130305', '');
INSERT INTO `sys_user_role` VALUES ('1320899195875360768', '1320899195225243649', '');
INSERT INTO `sys_user_role` VALUES ('1329795580615983104', '1329795579919728641', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1329795580615983105', '1329795579919728641', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1329795614484987904', '1329795613730013185', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1329795688124383232', '1329795687465877505', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1329795704863850496', '1329795703882383361', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1329795716930863104', '1329795716255580161', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1329795741211688960', '1329795740536406017', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1349021014649995264', '1349016976730619905', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1349021167326855168', '1349021166525743105', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1349021167326855169', '1349021166525743105', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1355967256000987136', '1355966975355912193', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1355967256000987137', '1355966975355912193', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1355967330718318592', '1355967204012589057', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1355967330718318593', '1355967204012589057', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1355967580686254080', '1355967579994193921', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1355967580686254081', '1355967579994193921', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1360858458609418240', '1309861917694623744', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1360858458609418241', '1309861917694623744', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1370973609278832640', '1370973608502886401', '1313761100243664896');
INSERT INTO `sys_user_role` VALUES ('1394919093030617088', '1306230031168569344', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1394923407102967808', '1394923406683537408', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('1394925051114946560', '1394925050699710464', '1309851245195821056');
INSERT INTO `sys_user_role` VALUES ('442110794142978048', null, '1');
INSERT INTO `sys_user_role` VALUES ('442110794142978049', null, '2');
INSERT INTO `sys_user_role` VALUES ('442110794142978050', null, '3');
INSERT INTO `sys_user_role` VALUES ('442114944884936704', '442114944884936704', '1');
INSERT INTO `sys_user_role` VALUES ('442114944884936705', '442114944884936704', '2');
INSERT INTO `sys_user_role` VALUES ('442114944884936706', '442114944884936704', '3');
INSERT INTO `sys_user_role` VALUES ('442114944884936707', '442114944884936704', '693913251020275712');
INSERT INTO `sys_user_role` VALUES ('442114944884936708', '442114944884936704', '693949793801601024');
INSERT INTO `sys_user_role` VALUES ('442114944884936709', '442114944884936704', '694106517393113088');
INSERT INTO `sys_user_role` VALUES ('442127724396548096', '3', '1');
INSERT INTO `sys_user_role` VALUES ('442127724396548097', '3', '2');
INSERT INTO `sys_user_role` VALUES ('442127724396548098', '3', '3');
INSERT INTO `sys_user_role` VALUES ('445004989551742976', '442492965651353600', '1');
INSERT INTO `sys_user_role` VALUES ('445004989551742977', '442492965651353600', '2');
INSERT INTO `sys_user_role` VALUES ('445005010271604736', '444226209941950464', '1');
INSERT INTO `sys_user_role` VALUES ('445005010271604737', '444226209941950464', '2');
INSERT INTO `sys_user_role` VALUES ('445005010271604738', '444226209941950464', '3');
INSERT INTO `sys_user_role` VALUES ('447196043407396864', '447196042723725312', '1');
INSERT INTO `sys_user_role` VALUES ('447196043407396865', '447196042723725312', '2');
INSERT INTO `sys_user_role` VALUES ('447197132043194368', '447197131518906368', '1');
INSERT INTO `sys_user_role` VALUES ('447197773046091776', '447197772274339840', '1');
INSERT INTO `sys_user_role` VALUES ('447200144400715776', '447199996320813056', '1');
INSERT INTO `sys_user_role` VALUES ('447200144400715777', '447199996320813056', '2');
INSERT INTO `sys_user_role` VALUES ('449248198469488640', '449248198058446848', '3');
INSERT INTO `sys_user_role` VALUES ('463926002653990912', '463926002318446592', '3');
INSERT INTO `sys_user_role` VALUES ('463926371165540352', '442488661347536896', '3');
