package com.pearadmin.api.modules;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.db.Entity;
import com.google.common.collect.Lists;
import com.pearadmin.common.websocket.UserDao;
import com.pearadmin.secure.session.SecureSessionService;
import com.pearadmin.system.service.ISysLogService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.pearadmin.common.plugin.logging.aop.annotation.Logging;
import com.pearadmin.common.plugin.logging.aop.enums.BusinessType;
import com.pearadmin.common.tools.secure.SecurityUtil;
import com.pearadmin.common.web.base.BaseController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Describe: 入 口 控 制 器
 * Author: 就 眠 仪 式
 * CreateTime: 2019/10/23
 * */
@RestController
@RequestMapping
@Api(tags = {"项目入口"})
public class EntranceController extends BaseController {

    @Resource
    private SessionRegistry sessionRegistry;

    @Resource
    private ISysLogService sysLogService;

    @Value("${server.port}")
    private int port;
    /**
     * Describe: 获取登录视图
     * Param: ModelAndView
     * Return: 登录视图
     * */
    @GetMapping("login")
    public ModelAndView login(HttpServletRequest request){
        if (SecurityUtil.isAuthentication()) {
            SecureSessionService.expiredSession(request, sessionRegistry);
            return jumpPage("index");
        }else{
            return jumpPage("login");
        }
    }

    /**
     * Describe: 获取主页视图
     * Param: ModelAndView
     * Return: 登录视图
     * */
    @GetMapping("index")
    @Logging(title = "主页",describe = "返回 Index 主页视图",type = BusinessType.ADD)
    public ModelAndView index( )
    {
        return jumpPage("index");
    }

    /**
     * Describe: 获取主页视图
     * Param: ModelAndView
     * Return: 主页视图
     * */
    @GetMapping("console")
    public ModelAndView home(HttpServletRequest request,Model model) throws UnknownHostException {
        UserDao dao=new UserDao(port);
        List<Entity> lastPeopleTime2 = dao.getLastPeopleTime2();
        if (CollectionUtil.isNotEmpty(lastPeopleTime2)) {

            String st1="",st2="",st3="",st4="",st5="",st6="";
            List<String> names = Lists.newArrayList();
            Entity eyD = lastPeopleTime2.get(0);
            st1=eyD.getStr("time");
            st2=eyD.getStr("start");
            st3=eyD.getStr("stop");
            st4=eyD.getStr("people1");
            st5=eyD.getStr("people2");
            st6=eyD.getStr("text").replaceAll("#", ",").substring(0,eyD.getStr("text").length()-1);
            String[] st66 = eyD.getStr("text").split("#");
            List lists = Arrays.asList(st66);
            names.addAll(lists);

            model.addAttribute("st1", st1);
            model.addAttribute("st2", st2);
            model.addAttribute("st3", st3);
            model.addAttribute("st4", st4);
            model.addAttribute("st5", st5);
            model.addAttribute("b", Integer.parseInt(st4)-Integer.parseInt(st5));
            model.addAttribute("st6", st6);
            model.addAttribute("names", names);

            //正序
            Collections.reverse(lastPeopleTime2);

            List<String> p1= Lists.newArrayList(),p2=Lists.newArrayList(),p3=Lists.newArrayList();
            for(Entity ey : lastPeopleTime2){
                int f=Integer.parseInt(ey.getStr("people1"))-Integer.parseInt(ey.getStr("people2"));
                p1.add(f+"");
                p2.add(ey.getStr("people2"));
                p3.add(ey.getStr("time")+" "+ey.getStr("start"));
            }
            model.addAttribute("p1", p1);
            model.addAttribute("p2", p2);
            model.addAttribute("p3", p3);
        }

        model.addAttribute("sessionId", request.getSession().getId());
        model.addAttribute("hosts", InetAddress.getLocalHost().getHostAddress()+":"+port);
        return jumpPage("console/console");
    }

    /**
     * Describe:无权限页面
     * Return:返回403页面
     * */
    @GetMapping("error/403")
    public ModelAndView noPermission(){
        return jumpPage("error/403");
    }

    /**
     * Describe:找不带页面
     * Return:返回404页面
     * */
    @GetMapping("error/404")
    public ModelAndView notFound(){
        return jumpPage("error/404");
    }

    /**
     * Describe:异常处理页
     * Return:返回500界面
     * */
    @GetMapping("error/500")
    public ModelAndView onException(){
        return jumpPage("error/500");
    }

}
