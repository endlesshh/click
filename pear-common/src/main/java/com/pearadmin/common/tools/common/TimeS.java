package com.pearadmin.common.tools.common;

public class TimeS {
    public static int getNowS(String time){

        String hour=time.substring(11,13);
        String f=time.substring(14,16);
        String m=time.substring(17,19);
        int a = Integer.parseInt(hour);
        int b = Integer.parseInt(f);
        int c = Integer.parseInt(m);
        int num=a*60*60+b*60+c;
        return num;
    }
    public static int getLastS(String time){
        String hour=time.substring(0,2);
        String f=time.substring(3,5);
        //System.out.println(hour);
        //System.out.println(f);
        int a = Integer.parseInt(hour);
        int b = Integer.parseInt(f);
        int num=a*60*60+b*60;
        return num;
    }
}
