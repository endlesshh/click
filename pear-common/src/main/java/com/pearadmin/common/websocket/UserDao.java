package com.pearadmin.common.websocket;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import com.google.common.collect.Lists;
import com.google.common.collect.Range;
import com.pearadmin.common.tools.common.TimeS;
import com.pearadmin.common.tools.sequence.SequenceUtil;
import com.pearadmin.common.web.domain.response.Result;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;

import java.net.InetAddress;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;


public class UserDao {
	private static final String t_a = "cmc_";
	private static final String t_b = "_data";
	private static final String t_c = "clevermc";
	private int port=8080;

	public UserDao(int port){
		this.port = port;
	}
	/**
	 * 获取签到范围
	 * @return
	 * @throws SQLException
	 */
	public List<Entity> getTimeRange() {
		try{
			String QUERY = "select * from sc_time";
			List<Entity> query = Db.use().query(QUERY);
			if (CollectionUtil.isNotEmpty(query)) {
				for(Entity et : query){
					et.put("range",Range.closed(TimeS.getLastS(et.getStr("start")), TimeS.getLastS(et.getStr("stop"))));
				}
			}
			return query;
		}catch (Exception e){
			e.printStackTrace();
		}
		return Lists.newArrayList();
	}

	/**
	 * //得到签到时间表
	 * @return
	 * @throws SQLException
	 */
	public List<Entity> getTime2() throws SQLException {
		String QUERY = "select * from sc_time";
		return Db.use().query(QUERY);
	}

	/**
	 * 判断当前的考勤记录是否已经创建
	 * @param time
	 * @param start
	 * @param stop
	 * @return
	 */
	public boolean  queryByLastPeopleTime(String time,String start,String stop) {
		String QUERY = "select * from sc_last_people_time where time=? and start=? and stop=?";
		try {
			List<Entity> query = Db.use().query(QUERY,time,start,stop);
			if (query != null && query.size() > 0) {
				return  true;
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return false;
	}
	public Entity  getLastPeopleTime(){
		try {
			String QUERY = "select * from sc_last_people_time ORDER BY id DESC LIMIT 1;";
			return Db.use().queryOne(QUERY);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean updateLastPeopleTime(int people1,int people2,String text,String time,String start,String stop){
		String updateA="update sc_last_people_time set people1=?,people2=?,text=? where time=? and start=? and stop=?";


		try {
			int a= Db.use().execute(updateA, people1, people2, text, time, start, stop);
			if(a>0){
				System.out.println("更新成功");
				return  true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return  false;
	}
	public int getPeopleNum() {//得到签到表人数
		String QUERY = "select * from sc_name_url";

		int i=0;
		try {
			List<Entity> query = Db.use().query(QUERY);
			if (CollectionUtil.isNotEmpty(query)) {
				i = query.size();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return i;
	}

	// 插入新的考勤记录
	public boolean insertLastPeopleTime(String time,String start, String stop,int yd,int sd,String text) {
		String INSERT = "insert into sc_last_people_time(time,start,stop,people1,people2,text) values(?,?,?,?,?,?)";
		try {
			int a= Db.use().execute(INSERT, time, start, stop, yd, sd, text);
			if(a>0){
				System.out.println("插入更新成功");
				return  true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
		return  false;
	}
	public List<Entity> getLastPeopleTime2() { //得到签到人员列表
		String QUERY = "select * from sc_last_people_time order by id desc  LIMIT 0,7;";
		try {
			return Db.use().query(QUERY);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getPeople() {
		//得到考勤人名单
		String QUERY = "select * from sc_name_url";
		String s1="",s2="",s3="";
		try {
			List<Entity> query = Db.use().query(QUERY);
			if (CollectionUtil.isNotEmpty(query)) {
				for(Entity rs : query){
					s1=s1+rs.getStr("name")+"#";
					s2=s2+ "http://"+InetAddress.getLocalHost().getHostAddress()+":"+port+"/system/file/download/"+rs.getStr("url")+"#";
					s3=s3+rs.getStr("xh")+"#";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		s1=s1.substring(0,s1.length()-1);
		s2=s2.substring(0,s2.length()-1);
		s3=s3.substring(0,s3.length()-1);
		return s1+"@"+s2+"@"+s3;
	}
	public boolean checkLogin(String uname,String pwd) {
		String QUERY = "select * from sys_user where username=?";
		try {
			List<Entity> query = Db.use().query(QUERY,uname);
			if (CollectionUtil.isNotEmpty(query)) {
				return new BCryptPasswordEncoder().matches(pwd, query.get(0).getStr("password"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static void main(String[] args) {
		//$2a$10$6T.NGloFO.mD/QOAUelMTOcjAH8N49h34TsXduDVlnNMrASIGBNz6
		System.out.println(new BCryptPasswordEncoder().encode("admin"));
		System.out.println(new BCryptPasswordEncoder().matches("admin", new BCryptPasswordEncoder().encode("admin")));
	}
}
