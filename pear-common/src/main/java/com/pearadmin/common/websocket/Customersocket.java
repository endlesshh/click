package com.pearadmin.common.websocket;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;

import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;


public class Customersocket {
	private String s;
	private int port;
	public Customersocket(String s,int port){
		this.s=s;
		this.port = port;
	}
	public void send() throws Exception {
			WebSocketClient client;
			client = new WebSocketClient(new URI("ws://"+InetAddress.getLocalHost().getHostAddress()+":"+port+"/WebSocket/server"), new Draft_6455()) {
			
			@Override
			public void onOpen(ServerHandshake arg0) {
				send(s);
				close();
			}
			@Override
			public void onMessage(String arg0) {
				System.out.println( "received: " + arg0 );
			}
			@Override
			public void onError(Exception arg0) {
				arg0.printStackTrace();
			}
			@Override
			public void onClose(int arg0, String arg1, boolean arg2) {
				try {
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		client.connect();
	}
	public String getS() {
		return s;
	}
	public void setS(String s) {
		this.s = s;
	}
	
}
